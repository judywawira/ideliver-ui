export const fetchUrl = url =>
  fetch(url, {
    credentials: 'same-origin',
  }).then(response => response.json());
