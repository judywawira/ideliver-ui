import React from 'react';
import Proptypes from 'prop-types';
import styles from './vcAlertsCard.scss';
import { List, ListItem, ListItemText } from 'material-ui-next/List';
import Divider from 'material-ui-next/Divider';
import messages from '../../intl/messages';
import { injectIntl } from 'react-intl';

/**
 * A component to present a list of alert messages
 * @param {*} props
 */

const VcAlertsCard = (props) => {
        const { formatMessage } = props.intl;
        // map each element in alert list to <ListItem>
        const alerts1 = props.alerts.map(alert => (
            <div key={alert.timestamp}>
            <ListItem button>
            <ListItemText secondary={alert.timestamp} primary={alert.value}/>
            </ListItem>
            <Divider />
            </div>
        ));
    return(
        <div className={styles.container}>
            <div className={styles.left}> {formatMessage(messages.inlineAlerts)} </div>
            <div className={styles.right}>{alerts1}</div>
        </div>
        );
}

VcAlertsCard.propTypes = {
  /** Array of alerts to be displayed */
  alerts: Proptypes.arrayOf(Proptypes.shape({
      /* timestamp of the alert - defined as String */
    timestamp: Proptypes.string,
    /* alert message text - defined as String */
    value: Proptypes.string,
  })),
};

VcAlertsCard.defaultProps = {
  alerts: [],
};

export default injectIntl(VcAlertsCard);