import React from 'react';
import Proptypes from 'prop-types';
import cx from 'classnames';
import Button from 'material-ui/Button';
import styles from './vcButton.scss';
import AddIcon from 'material-ui-icons-next/Add';
import RemoveIcon from 'material-ui-icons-next/Remove';

/**
 * Button component to be used for all standard buttons
 * @param {*} props
 */
const VcButton = (props) => {
  const buttonStyle = cx(styles.button, {
    [styles.buttonBackground]: props.isSelected || props.type === 'primary',
    [styles.buttonBackgroundColorPrimarySelected]: props.isSelected && props.type === 'primary',
  });

  const labelStyle = cx(styles.label, {
    [styles.labelColor]: props.isSelected || props.type === 'primary',
    [styles.labelColorPrimarySelected]: props.type === 'primary' && props.isSelected,
  });

  return (
    <Button
      onClick={props.onClick}
      className={buttonStyle}
      variant="raised"
      classes={{ label: labelStyle }}
      disabled={props.disabled}
    >
      {props.value}
    </Button>
  );
};

VcButton.propTypes = {
  /** Boolean to show if the button is disabled */
  disabled: Proptypes.bool,
  /** The text or icon to be displayed in the button */
  value: Proptypes.node,
  /** Callback function triggered on click */
  onClick: Proptypes.func,
  /** Boolean to show if button is selected or not */
  isSelected: Proptypes.bool,
  /** button type can be either of primary or secondary */
  type: Proptypes.oneOf(['primary', 'secondary']).isRequired,
};

VcButton.defaultProps = {
  disabled: false,
  value: '',
  type: 'primary',
  isSelected: false,
};

export default VcButton;
