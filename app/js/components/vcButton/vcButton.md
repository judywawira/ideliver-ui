VcButton example:

```js
<VcGridRow>
  <VcButton value="primary not selected" type="primary" />
  <VcButton value="secondary selected" type="secondary" isSelected />
  <VcButton value="secondary not selected" type="secondary" />
  <VcButton value="primary selected" type="primary" isSelected />
  <VcButton value="Disabled" disabled />
</VcGridRow>
```
