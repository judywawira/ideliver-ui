import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import { injectIntl } from 'react-intl';
import DiagnosisIcon from '../svgIcon/diagnosisIcon';
import NoteIcon from '../svgIcon/noteIcon';
import PlusCircleIcon from '../svgIcon/plusCircleIcon';
import CrossCircleIcon from '../svgIcon/crossCircleIcon';
import VcDialog from '../vcDialog/vcDialog';
import VcDialogContentDiagnosis from '../vcDialogContentDiagnosis/vcDialogContentDiagnosis';
import VcDialogContentSummary from '../vcDialogContentSummary/vcDialogContentSummary';
import VcDiagnosisCardContent from '../vcDiagnosisCardContent/vcDiagnosisCardContent';
import styles from './vcCardWithDialog.scss';
import messages from '../../intl/messages';

class VcCardWithDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      open: false,
      comment: '',
      index: this.props.filterComments ? this.props.filterComments.length : 0,
    };
  }

  /* handles change of circle icon when close icon is clicked */
  handleImageChange = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
      open: true,
      comment: '',
      index: this.props.filterComments ? this.props.filterComments.length : 0,
    }));
  };

  /* handles close of dialog when close button is clicked */
  handleDialogClose = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
      open: false,
    }));
  };

  handleCommentChange = (comment) => {
    this.props.onAddComment(comment, this.state.index);
    if (!comment) {
      this.setState(() => ({
        index: this.props.filterComments ? this.props.filterComments.length : 0,
      }));
    }
  };

  /* handles opening of dialog when edit is clicked */
  openEditCommentDialog = (e, i) => {
    this.setState({
      open: true,
      comment: e,
      expanded: true,
      index: i,
    });
  };

  render() {
    const { formatMessage } = this.props.intl;
    const header = cx(styles.header, {
      [styles.backgroundColorGreen]: this.props.value === 'Possible Diagnosis',
      [styles.backgroundColorPurple]: this.props.value === 'Diagnosis',
      [styles.backgroundColorGrey]: this.props.value === 'Summary',
    });

    /* handles display of dialog component based on state : expanded */
    const displayDialog =
      this.state.expanded === true && this.props.value === 'Summary' ? (
        <VcDialog
          title="Add summary"
          type={this.props.value}
          open={this.state.open}
          onToggle={this.handleDialogClose}
          dialogContent={
            <VcDialogContentSummary
              type={this.props.value}
              filterComments={this.props.filterComments}
              onComment={this.handleCommentChange}
              comment={this.state.comment}
              index={this.state.index}
            />
          }
        />
      ) : this.state.expanded === true ? (
        <VcDialog
          title={
            this.props.value === 'Possible Diagnosis' ? 'Add possible diagnosis' : 'Add diagnosis'
          }
          type={this.props.value}
          open={this.state.open}
          onToggle={this.handleDialogClose}
          dialogContent={
            <VcDialogContentDiagnosis
              type={this.props.value}
              filtersSelected={this.props.filtersSelected}
              filterOptions={this.props.filterOptions}
              onSelection={this.props.onFiltersSelected}
            />
          }
        />
      ) : null;

    return (
      <div>
        <Card className={styles.card}>
          <CardHeader
            classes={{
              title: styles.title,
              action: styles.action,
            }}
            title={this.props.title}
            className={header}
            action={
              <IconButton onClick={this.handleImageChange}>
                {this.state.expanded === false ? <PlusCircleIcon /> : <CrossCircleIcon />}
              </IconButton>
            }
          />

          {this.props.value === 'Summary' ? (
            <VcDiagnosisCardContent
              value={this.props.value}
              filterComments={this.props.filterComments}
              editComment={this.openEditCommentDialog}
            />
          ) : (
            <VcDiagnosisCardContent
              value={this.props.value}
              filterOptions={this.props.filterOptions}
              filtersSelected={this.props.filtersSelected}
            />
          )}

          {displayDialog}
        </Card>
      </div>
    );
  }
}

VcCardWithDialog.propTypes = {
  /** title of the card */
  title: PropTypes.string.isRequired,
  /** type of the card */
  value: PropTypes.oneOf(['Diagnosis', 'Possible Diagnosis', 'Summary']),
  /** Complete list of options for diagnosis */
  filterOptions: PropTypes.array,
  /** List of comments added for summary */
  filterComments: PropTypes.array,
  /** List of diagnosis added */
  filtersSelected: PropTypes.array,
  /** Callback function for when a filter is selected */
  onFiltersSelected: PropTypes.func,
  /** Callback function for when a comment is added */
  onAddComment: PropTypes.func,
};

VcCardWithDialog.defaultProps = {
  filtersSelected: [],
  filterOptions: [],
  filterComments: [],
};

export default injectIntl(VcCardWithDialog);
