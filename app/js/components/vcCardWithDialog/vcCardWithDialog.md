VcCardWithDialog example:

```js
<div>
  <VcCardWithDialog
    title="Possible diagnosis"
    value="Possible Diagnosis"
    filterOptions={[
      "Pre-eclampsia",
      "Infection",
      "Anemia",
      "Erythrocytosis",
      "Shock",
      "Test1",
      "Test2",
      "Test3",
      "Test4",
      "Test5",
      "Shock2",
      "Test12",
      "Test22",
      "Test32",
      "Test42",
      "Test52"
    ]}
  />
  <VcCardWithDialog
    title="Diagnosis"
    value="Diagnosis"
    filterOptions={[
      "Pre-eclampsia",
      "Infection",
      "Anemia",
      "Erythrocytosis",
      "Shock",
      "Test1",
      "Test2",
      "Test3",
      "Test4",
      "Test5",
      "Shock2",
      "Test12",
      "Test22",
      "Test32",
      "Test42",
      "Test52"
    ]}
  />
  <VcCardWithDialog title="Visit Summary" value="Summary" />
</div>
```
