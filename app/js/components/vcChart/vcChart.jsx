import React from 'react';
import Proptypes from 'prop-types';
import {
  LineChart,
  Line,
  Brush,
  ReferenceLine,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  LabelList,
} from 'recharts';
import VcChartDot from './vcChartDot/vcChartDot';
import VcChartLegend from './vcChartLegend/vcChartLegend';
import VcChartTick from './vcChartTick/vcChartTick';
import VcChartTooltip from './vcChartTooltip/vcChartTooltip';
import VcChartLabel from './vcChartLabel/vcChartLabel';
import styles from './vcChart.scss';

const chartWidth = 600;
const chartHeight = 300;

/**
 * Chart component to represent array of objects on a line chart
 * @param {*} props
 */
const VcChart = props => (
  <LineChart
    fontFamily="Roboto"
    width={chartWidth}
    height={chartHeight}
    data={props.value}
    margin={{
      top: 5,
      right: 50,
      left: 20,
      bottom: 5,
    }}
  >
    <XAxis dataKey={props.yDataKey} tick={<VcChartTick />} />
    <YAxis
      type="number"
      domain={[
        props.minValue ? props.minValue : `dataMin - ${20}`,
        props.maxValue ? props.maxValue : `dataMax + ${20}`,
      ]}
      scale="linear"
    />
    <CartesianGrid strokeDasharray="3 3" />
    <Tooltip content={VcChartTooltip} name="" separator="" labelFormatter={() => null} />
    <Legend
      verticalAlign="top"
      wrapperStyle={{ height: '34px', lineHeight: '34px', marginLeft: '60px' }}
      content={
        <VcChartLegend
          topLabelDataKey={props.topLabelDataKey}
          bottomLabelDataKey={props.bottomLabelDataKey}
        />
      }
    />
    <ReferenceLine y={0} stroke="#000" />
    <Brush dataKey={props.yDataKey} height={25} stroke={styles.seafoam_blue} y={chartHeight - 25} />
    <Line
      dataKey={props.xDataKey}
      stroke={styles.seafoam_blue}
      strokeWidth={3.5}
      dot={<VcChartDot alertValue={props.alertValue} />}
      activeDot={false}
    >
      {props.topLabelDataKey ? (
        <LabelList
          dataKey={props.topLabelDataKey}
          position="top"
          content={VcChartLabel}
          colors={props.topLabelColors}
        />
      ) : null}
      {props.bottomLabelDataKey ? (
        <LabelList
          dataKey={props.bottomLabelDataKey}
          position="bottom"
          content={VcChartLabel}
          colors={props.bottomLabelColors}
        />
      ) : null}
    </Line>
  </LineChart>
);

VcChart.propTypes = {
  /** The array of objects that will be displayed */
  value: Proptypes.array,
  /** The array of objects that will be displayed */
  alertValue: Proptypes.number,
  /** min value for the range of the chart */
  minValue: Proptypes.number,
  /** max value for the range of the chart */
  maxValue: Proptypes.number,
  /** the key that represents the x-coordinate */
  xDataKey: Proptypes.string,
  /** the key that represents the y-coordinate */
  yDataKey: Proptypes.string,
  /** the key that represents the label above a point */
  topLabelDataKey: Proptypes.string,
  /** the key that represents the label below a point */
  bottomLabelDataKey: Proptypes.string,
  /**
   * object in which the keys are topLabelDataKey values and
   * the values are colors that are to be used for the labels representing them
   */
  topLabelColors: Proptypes.object,
  /**
   * object in which the keys are bottomLabelDataKey values and
   * the values are colors that are to be used for the labels representing them
   */
  bottomLabelColors: Proptypes.object,
};

VcChart.defaultProps = {};

export default VcChart;
