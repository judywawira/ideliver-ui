import React from 'react';
import Proptypes from 'prop-types';
import styles from '../vcChart.scss';

/**
 * A dot to be used by VcChart
 * @param {*} props
 */
const VcChartDot = (props) => {
  const {
    cx, cy, stroke, payload, value, alertValue,
  } = props;

  const backgoundColor = (val, alertVal) => {
    switch (true) {
    case val < alertVal:
      return styles.seafoam_blue;
    case val >= alertVal:
      return styles.tomato;
    default:
      return styles.seafoam_blue;
    }
  };

  return value ? (
    <svg x={cx ? cx - 8 : null} y={cy ? cy - 8 : null} width={16} height={16}>
      <circle
        cx={8}
        cy={8}
        r="6"
        stroke={backgoundColor(value, alertValue)}
        strokeWidth="3.5"
        fill="white"
        strokeDasharray={props.payload && props.payload.irregular ? '11,8' : null}
        strokeDashoffset={props.payload && props.payload.irregular ? '8' : null}
      />
    </svg>
  ) : null;
};

VcChartDot.propTypes = {
  /** vulue of the dot */
  value: Proptypes.number,
  /** if value is >= to alertValue the dot will be in the alert color  */
  alertValue: Proptypes.number,
  /** the whole data object used to get info if the reading is irregular */
  payload: Proptypes.shape({ payload: Proptypes.shape({ irregular: Proptypes.bool }) }),
};

VcChartDot.defaultProps = {};

export default VcChartDot;
