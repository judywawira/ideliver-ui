import React from 'react';
import { injectIntl } from 'react-intl';
import Proptypes from 'prop-types';
import { Typography } from 'material-ui';
import VcGridRow from '../../vcGrid/vcGridRow/vcGridRow';
import messages from '../../../intl/messages';
import styles from './vcChartLegend.scss';
import chartStyles from '../vcChart.scss';

/**
 * Legent to be used by VcChart
 * @param {*} props
 */
const VcChartLegend = (props) => {
  const { formatMessage } = props.intl;

  return (
    <VcGridRow className={styles.container}>
      <svg width={16} height={16}>
        <circle
          cx={8}
          cy={8}
          r="6"
          stroke={chartStyles.seafoam_blue}
          strokeWidth="3.5"
          fill="white"
        />
      </svg>
      <Typography>{formatMessage(messages.normal)}</Typography>
      <svg width={16} height={16}>
        <circle cx={8} cy={8} r="6" stroke={chartStyles.tomato} strokeWidth="3.5" fill="white" />
      </svg>
      <Typography>{formatMessage(messages.alert)}</Typography>
      <svg width={16} height={16}>
        <circle
          cx={8}
          cy={8}
          r="6"
          stroke="lightGray"
          strokeWidth="3.5"
          fill="white"
          strokeDasharray="11,8"
          strokeDashoffset="8"
        />
      </svg>
      <Typography>{formatMessage(messages.irregular)}</Typography>
      {props.topLabelDataKey
        ? [
          <svg width={26} height={26} key={`${props.topLabelDataKey}0`}>
            <g>
              <rect
                x={0}
                y={0}
                rx={20}
                ry={20}
                height={20 * 1.3}
                width={20 * 1.3}
                fill="lightGray"
              />
            </g>
          </svg>,
          <Typography key={`${props.topLabelDataKey}1`}>
            {formatMessage(messages[props.topLabelDataKey])}
          </Typography>,
        ]
        : null}
      {props.bottomLabelDataKey
        ? [
          <svg key={`${props.bottomLabelDataKey}0`} width={40} height={20}>
            <g>
              <rect
                x={0}
                y={0}
                rx={20 * 0.25}
                ry={20 * 0.25}
                height={20}
                width={20 * 2}
                fill="lightGray"
              />
            </g>
          </svg>,
          <Typography key={`${props.bottomLabelDataKey}1`}>
            {formatMessage(messages[props.bottomLabelDataKey])}
          </Typography>,
        ]
        : null}
    </VcGridRow>
  );
};

VcChartLegend.propTypes = {
  /** If set a label will show in the legend with the message defined with the same id */
  topLabelDataKey: Proptypes.string,
  /** If set a label will show in the legend with the message defined with the same id */
  bottomLabelDataKey: Proptypes.string,
};

VcChartLegend.defaultProps = {};

export default injectIntl(VcChartLegend);
