import React from 'react';
import { Paper, Typography } from 'material-ui';
import styles from './vcChartTooltip.scss';

/**
 * Custom Tooltip to be used by VcChart that show all the information from the payload
 * @param {*} props
 */
const VcChartTooltip = (props) => {
  const { active, payload } = props;

  if (active && payload && payload.length > 0) {
    return (
      <Paper elevation={4} className={styles.paper}>
        {Object.keys(payload[0].payload).map(key => (
          <Typography key={key} color="textSecondary">
            {`${key.charAt(0).toUpperCase() + key.slice(1)} : ${
              payload[0].payload[key] === true ? 'Yes' : payload[0].payload[key]
            }`}
          </Typography>
        ))}
      </Paper>
    );
  }
  return null;
};

VcChartTooltip.propTypes = {};

VcChartTooltip.defaultProps = {};

export default VcChartTooltip;
