import React from "react";
import Proptypes from "prop-types";
import cx from "classnames";
import { injectIntl } from "react-intl";
import Paper from "material-ui/Paper";
import { Typography, Chip } from "material-ui";
import { CheckCircle } from "material-ui-icons-next";
import styles from "./vcClientCard.scss";
import messages from "../../intl/messages";
import VcTimer, { TIMER_OPTIONS_ENUM } from "../vcTimer/vcTimer";
import VcAcuity from "../vcAcuity/vcAcuity";
import VcPatientStatus, {
  PATIENT_STATUS_ENUM
} from "../vcPatientStatus/vcPatientStatus";
import VcGrid from "../vcGrid/vcGrid";
import VcGridRow from "../vcGrid/vcGridRow/vcGridRow";
import VcGridColumn from "../vcGrid/vcGridColumn/vcGridColumn";
import VcDatapoint from "../vcDatapoint/vcDatapoint";

/**
 * A card to present the Patient's/Client's information.
 * @param {*} props
 */
const VcClientCard = props => {
  const { formatMessage } = props.intl;

  const riskFactors = props.riskFactors
    ? props.riskFactors.map(item => (
        <Chip key={item} className={styles.riskChip} label={item} />
      ))
    : null;

  const complaints = props.complaints
    ? props.complaints.map(item => (
        <Chip key={item} className={styles.complaintChip} label={item} />
      ))
    : null;

  return (
    <Paper elevation={24} className={cx(styles.card, props.className)}>
      <img
        className={styles.media}
        src={props.img}
        alt={`${props.givenName} ${props.familyName}`}
      />
      <VcGrid className={styles.gridContainer}>
        <VcGridRow flex={1} className={styles.nameWeekRow}>
          <VcGridColumn flex={3}>
            <Typography variant="headline" gutterBottom>
              {`${props.givenName} ${props.familyName}`}
            </Typography>
          </VcGridColumn>
          <VcGridRow flex={1}>
            <Typography variant="subheading">
              {formatMessage(messages.week)}
            </Typography>
            <div className={styles.weekBox}>{props.weeks}</div>
          </VcGridRow>
        </VcGridRow>
        <VcGridRow flex={1}>
          <VcGridColumn flex={1}>
            <VcDatapoint label={formatMessage(messages.age)}>
              {props.age}
            </VcDatapoint>
          </VcGridColumn>
          <VcGridColumn flex={4}>
            <VcDatapoint label={formatMessage(messages.companion)}>
              {`${props.companionGivenName} ${props.companionFamilyName}`}
            </VcDatapoint>
          </VcGridColumn>
          <VcGridColumn flex={4}>
            <VcDatapoint label={formatMessage(messages.education)}>
              {props.education}
            </VcDatapoint>
          </VcGridColumn>
          <VcGridRow flex={3}>
            <VcGridColumn>
              <VcDatapoint label={formatMessage(messages.gravidityAbrev)}>
                <div className={styles.colorPurple}>{props.gravidity}</div>
              </VcDatapoint>
            </VcGridColumn>
            <VcGridColumn>
              <VcDatapoint label={formatMessage(messages.pretermBirthsAbrev)}>
                <div className={styles.colorPurple}>{props.pretermBirths}</div>
              </VcDatapoint>
            </VcGridColumn>
            <VcGridColumn>
              <VcDatapoint label={formatMessage(messages.termBirthsAbrev)}>
                <div className={styles.colorPurple}>{props.termBirths}</div>
              </VcDatapoint>
            </VcGridColumn>
            <VcGridColumn>
              <VcDatapoint label={formatMessage(messages.abortionsAbrev)}>
                <div className={styles.colorPurple}>{props.abortions}</div>
              </VcDatapoint>
            </VcGridColumn>
            <VcGridColumn>
              <VcDatapoint label={formatMessage(messages.livingChildrenAbrev)}>
                <div className={styles.colorPurple}>{props.livingChildren}</div>
              </VcDatapoint>
            </VcGridColumn>
            <VcGridColumn flex={3} />
          </VcGridRow>
        </VcGridRow>
        <VcGridRow flex={2} className={styles.ancComplaintsRow}>
          <VcGridColumn flex={2}>
            <VcDatapoint label={formatMessage(messages.ancRisk)}>
              {riskFactors}
            </VcDatapoint>
          </VcGridColumn>
          <VcGridColumn flex={2} className={styles.complaintsColumn}>
            <VcDatapoint label={formatMessage(messages.presentingComplaints)}>
              {complaints}
            </VcDatapoint>
          </VcGridColumn>
        </VcGridRow>
        <VcGridRow flex={1}>
          <VcGridRow className={styles.taskRow}>
            {props.task
              ? [
                  <CheckCircle className={styles.checkCircle} />,
                  <div className={styles.task}>
                    {formatMessage(messages.task)}
                    {":"}
                  </div>
                ]
              : null}

            {props.task}
          </VcGridRow>
        </VcGridRow>
      </VcGrid>
      <div className={styles.rightSection}>
        <VcAcuity value={props.acuity} withLabel />
        <VcTimer value={props.nextCheckUp} />
        <VcPatientStatus value={props.patientStatus} />
      </div>
    </Paper>
  );
};
VcClientCard.propTypes = {
  /** Picture of the patient */
  img: Proptypes.string,
  /** given name of the patient */
  givenName: Proptypes.string,
  /** family name of the patient */
  familyName: Proptypes.string,
  /** age name of the patient */
  age: Proptypes.number,
  /** companion's given name */
  companionGivenName: Proptypes.string,
  /** companion's family name */
  companionFamilyName: Proptypes.string,
  /** education level of the patient */
  education: Proptypes.string,
  /** weeks of the pregnancy */
  weeks: Proptypes.number,
  /** gravidity */
  gravidity: Proptypes.number,
  /** number of pervious preterm births */
  pretermBirths: Proptypes.number,
  /** number of pervious term births */
  termBirths: Proptypes.number,
  /** number of abortions */
  abortions: Proptypes.number,
  /** number of living children */
  livingChildren: Proptypes.number,
  /** array of risk factors for the patient */
  riskFactors: Proptypes.arrayOf(Proptypes.string),
  /** array of complaints for the patient */
  complaints: Proptypes.arrayOf(Proptypes.string),
  /** task that needs to be done for the patient */
  task: Proptypes.string,
  /** number representing acuity level */
  acuity: Proptypes.number,
  /** status of the patient */
  patientStatus: Proptypes.oneOf(PATIENT_STATUS_ENUM),
  /** time till next checkup */
  nextCheckUp: Proptypes.oneOf(TIMER_OPTIONS_ENUM)
};

VcClientCard.defaultProps = {
  img: "img/ideliver-gradient-01.png"
};

export default injectIntl(VcClientCard);
