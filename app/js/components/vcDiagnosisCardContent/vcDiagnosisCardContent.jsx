import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { CardContent } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import { injectIntl } from 'react-intl';
import DiagnosisIcon from '../svgIcon/diagnosisIcon';
import NoteIcon from '../svgIcon/noteIcon';
import styles from './vcDiagnosisCardContent.scss';
import messages from '../../intl/messages';

class VcDiagnosisCardContent extends React.Component {
  render() {
    const { formatMessage } = this.props.intl;

    const cardContent =
      this.props.value === 'Diagnosis'
        ? formatMessage(messages.cardContentDiagnosis)
        : this.props.value === 'Possible Diagnosis'
          ? formatMessage(messages.cardContentPossibleDiagnosis)
          : formatMessage(messages.cardContentSummary);

    const main = cx({
      [styles.backgroundColorIconGreen]: this.props.value === 'Possible Diagnosis',
      [styles.backgroundColorIconPurple]: this.props.value === 'Diagnosis',
    });

    const label = cx(styles.label, {
      [styles.backgroundColorLabelGreen]: this.props.value === 'Possible Diagnosis',
      [styles.backgroundColorLabelPurple]: this.props.value === 'Diagnosis',
    });

    const getDiagnosisIcon = <DiagnosisIcon />;

    const getSummaryIcon = (filter, index) => (
      <IconButton onClick={() => this.props.editComment(filter, index)}>
        <NoteIcon />
      </IconButton>
    );

    const getOptions = filters =>
      (filters.length > 0 ? (
        filters.map((filter, index) => (
          <div className={styles.fullRow} key={index}>
            <div className={styles.row}>
              <div className={main}>
                {this.props.value === 'Summary' ? getSummaryIcon(filter, index) : getDiagnosisIcon}
              </div>
              <label className={label}>
                {this.props.value === 'Summary' ? filter : this.props.filterOptions[filter]}
              </label>
              <br />
            </div>
            <Divider className={styles.divider} light />
          </div>
        ))
      ) : (
        <div className={styles.para}>
          <p>{cardContent}</p>
        </div>
      ));

    return (
      <CardContent className={styles.overflow}>
        {this.props.value === 'Summary'
          ? getOptions(this.props.filterComments)
          : getOptions(this.props.filtersSelected)}
      </CardContent>
    );
  }
}

VcDiagnosisCardContent.propTypes = {
  /** type of the card */
  value: PropTypes.oneOf(['Diagnosis', 'Possible Diagnosis', 'Summary']).isRequired,
  /** List of comments added for summary */
  filterComments: PropTypes.array,
  /** List of diagnosis added */
  filtersSelected: PropTypes.array,
  /** Complete list of options for diagnosis */
  filterOptions: PropTypes.array,
  /** callback function when edit button is clicked */
  editComment: PropTypes.func,
};

VcDiagnosisCardContent.defaultProps = {
  filtersSelected: [],
  filterOptions: [],
  filterComments: [],
};

export default injectIntl(VcDiagnosisCardContent);
