import React from "react";
import Button from "material-ui/Button";
import cx from "classnames";
import PropTypes from "prop-types";
import Dialog, { DialogActions, DialogTitle } from "material-ui/Dialog";
import styles from "./vcDialog.scss";
import messages from "../../intl/messages";

class VcDialog extends React.Component {
  render() {
    const dialogTitle = cx(styles.dialogTitle, {
      [styles.backgroundColorGreen]: this.props.type === "Possible Diagnosis",
      [styles.backgroundColorPurple]: this.props.type === "Diagnosis",
      [styles.backgroundColorGrey]: this.props.type === "Summary"
    });

    return (
      <div>
        <Dialog
          classes={{ paper: styles.paper }}
          open={this.props.open}
          keepMounted
          onClose={this.handleClose}
        >
          <DialogTitle className={dialogTitle}>
            <label className={styles.otherLabel}>
              <b>{this.props.title}</b>
            </label>
            <DialogActions>
              <Button
                className={styles.closeButton}
                onClick={this.props.onToggle}
              >
                <b> Close</b>
              </Button>
            </DialogActions>
          </DialogTitle>

          {this.props.dialogContent}
        </Dialog>
      </div>
    );
  }
}

VcDialog.propTypes = {
  /** title of dialog box */
  title: PropTypes.string.isRequired,
  /** type of dialog box */
  type: PropTypes.string.isRequired,
  /** default state of dialog box */
  open: PropTypes.bool,
  /** Call Back function to toggle state of dialog box */
  onToggle: PropTypes.func,
  /** content to be displayed inside dialog content */
  dialogContent: PropTypes.node
};

VcDialog.defaultProps = {
  open: false
};

export default VcDialog;
