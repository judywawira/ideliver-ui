VcDialog example:

```js
<VcDialog
  type="Diagnosis"
  title="Add diagnosis"
  open={false}
  filtersSelected={[0, 4, 5]}
  filterOptions={[
    "Pre-eclampsia",
    "Infection",
    "Anemia",
    "Erythrocytosis",
    "Shock",
    "Eye",
    "Anemia",
    "Erythrocytosis",
    "Shock"
  ]}
/>
```
