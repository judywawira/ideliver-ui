import React from "react";
import PropTypes from "prop-types";
import { DialogContent } from "material-ui/Dialog";
import VcButton from "../vcButton/vcButton";
import VcTextField from "../vcTextField/vcTextField";
import styles from "./vcDialogContentSummary.scss";

class VcDialogContentSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      summary: this.props.comment
    };
  }

  updateComment = value => {
    this.setState({
      summary: value
    });
  };

  render() {
    const dialogContent = (
      <div>
        <VcTextField
          value={this.props.comment}
          label="Add summary"
          onTextChange={this.updateComment}
        />
        <div className={styles.button}>
          <VcButton
            value="Submit"
            onClick={() =>
              this.props.onComment(this.state.summary, this.props.index)
            }
          />
        </div>
      </div>
    );

    return (
      <div>
        <DialogContent className={styles.dialogContent}>
          {dialogContent}
        </DialogContent>
      </div>
    );
  }
}

VcDialogContentSummary.propTypes = {
  /** type of dialog box */
  type: PropTypes.string,
  /** List of comments added for summary */
  filterComments: PropTypes.array,
  /** comment to be displayed in case of summary dialog */
  comment: PropTypes.string,
  /** index of the comment that needs to be updated */
  index: PropTypes.number,
  /** Call Back function when comment is added */
  onComment: PropTypes.func
};

VcDialogContentSummary.defaultProps = {
  type: "Summary",
  filterComments: [],
  comment: "",
  index: -1
};

export default VcDialogContentSummary;
