import React from "react";
import PropTypes from "prop-types";
import Proptypes from "prop-types";
import { MenuItem } from "material-ui/Menu";
import messages from "../../intl/messages";
import styles from "./vcGenericDropDown.scss";
import SelectField from "material-ui/Select";
/**
 * This component lets the user select an option from a drop-down list
 * @param {*} props
 */
const VcGenericDropDown = props => {
  const options = props.options.map(option => (
    <MenuItem key={option.value}>{option.value}</MenuItem>
  ));
  return (
    <div>
      <SelectField
        value={props.default}
        onChange={event => {
          props.onChange(event.target.value);
        }}
      >
        {options}
      </SelectField>
    </div>
  );
};

VcGenericDropDown.propTypes = {
  /** title of component */
  title: Proptypes.string,
  /** options for drop-down list - provided as an array */
  options: Proptypes.arrayOf(
    Proptypes.shape({
      value: Proptypes.string
    })
  ),
  /** Call Back function when value changes */
  onChange: Proptypes.func.isRequired
};

VcGenericDropDown.defaultProps = {
  onChange: () => {}
};

export default VcGenericDropDown;
