VcGenericDropDown example:

```js
<VcGenericDropDown
  default="select an option"
  options={[
    { value: "option 1" },
    { value: "option 2" },
    { value: "option 3" }
  ]}
/>
```
