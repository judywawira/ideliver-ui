import React from "react";
import Proptypes from "prop-types";
import cx from "classnames";
import styles from "./vcGenericSwitch.scss";
import { injectIntl } from "react-intl";
import messages from "../../intl/messages";
import { MenuItem } from "material-ui/Menu";
import VcGrid from "../vcGrid/vcGrid";
import VcGridRow from "../vcGrid/vcGridRow/vcGridRow";
import VcGridColumn from "../vcGrid/vcGridColumn/vcGridColumn";

/**
 * A component for switching true/false
 * @param {*} props
 */

class VcGenericSwitch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showComponents: false
    };
  }

  handleButtonClick = () => {
    this.setState(
      previousState => ({ showComponents: !previousState.showComponents }),
      () => {}
    );
  };

  render() {
    const { formatMessage } = this.props.intl;
    const nestedComponents = this.props.nestedComponents.map(
      (nestedComponent, index) => (
        <VcGridColumn className={styles.break} key={"switchComponent" + index}>
          {nestedComponent}
        </VcGridColumn>
      )
    );

    return (
      <VcGrid className={styles.gridContainer}>
        <VcGridRow>
          <label htmlFor="toggle" className={styles.sectionToggle}>
            <input
              type="checkbox"
              id="toggle"
              checked={this.props.value}
              onChange={() => this.props.onChange(!this.props.value)}
            />
            <div className={styles.slider}>
              <span className={styles.toggleCheckboxSlider} />
              <div
                onClick={this.handleButtonClick}
                className={cx(styles.sliderLabel, styles.no)}
              >
                {formatMessage(messages.toggleNo)}
              </div>
              <div
                onClick={this.handleButtonClick}
                className={cx(styles.sliderLabel, styles.yes)}
              >
                {formatMessage(messages.toggleYes)}
              </div>
            </div>
          </label>
        </VcGridRow>
        {this.state.showComponents ? (
          <VcGridRow>{nestedComponents}</VcGridRow>
        ) : null}
      </VcGrid>
    );
  }
}

VcGenericSwitch.propTypes = {
  /** a boolean to indicate if styling for invalid entry is to be set on the toggle button */
  invalid: Proptypes.bool,
  /** a boolean to indicate if the slider should be disabled */
  disabled: Proptypes.bool,
  /** value for the toggle button */
  value: Proptypes.bool,
  /** An array of nested components to be rendered */
  nestedComponents: Proptypes.arrayOf(Proptypes.node),
  /** callback function to be fired onChange */
  onChange: Proptypes.func
};

VcGenericSwitch.defaultProps = {
  invalid: false,
  disabled: false,
  value: false,
  onChange: () => {},
  nestedComponents: []
};

export default injectIntl(VcGenericSwitch);
