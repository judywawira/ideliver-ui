import React from "react";
import Proptypes from "prop-types";
import { Input, InputLabel, InputAdornment } from "material-ui-next/Input";
import { FormControl, FormHelperText } from "material-ui/Form";
import { injectIntl } from "react-intl";
import TextField from "material-ui/TextField";
import MenuItem from "material-ui/Menu";
/**
 * A text field that can support units, label, helper text and icons
 *
 */
const VcGenericTextField = props => {
  return (
    <div>
      <FormControl fullWidth>
        <TextField
          type={props.vType}
          label={props.label}
          value={props.value}
          onChange={event => props.onChange(event.target.value)}
          inputProps={{
            maxLength: props.maxLength,
            pattern: props.pattern
          }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">{props.tail}</InputAdornment>
            ),
            startAdornment: (
              <InputAdornment position="start">{props.head}</InputAdornment>
            )
          }}
        />
        <FormHelperText>{props.helperText}</FormHelperText>
      </FormControl>
    </div>
  );
};

VcGenericTextField.propTypes = {
  /** value to be displayed in the text field */
  value: Proptypes.string,
  /** type of values to be entered - text/number */
  vType: Proptypes.oneOf(["text", "number"]),
  /** label to be displayed in the text field */
  label: Proptypes.string,
  /** text at the end of the field - used to specify units */
  tail: Proptypes.string,
  /** text/icon at the start of the field - passed as a node */
  head: Proptypes.node,
  /** text below the input field - used to show errors/ description of the field */
  helperText: Proptypes.string,
  /** max number of characters allowed in the text field area */
  maxLength: Proptypes.number,
  /** Call Back function when value changes */
  onChange: Proptypes.func.isRequired,
  /** regex pattern */
  pattern: Proptypes.string
};

VcGenericTextField.defaultProps = {
  value: "",
  maxLength: 200,
  label: "",
  vType: "text",
  onChange: () => {}
};

export default injectIntl(VcGenericTextField);
