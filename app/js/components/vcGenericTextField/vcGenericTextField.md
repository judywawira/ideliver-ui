VcGenericTextField example:

```js
<VcGenericTextField
  value="Test comments"
  label="Title"
  pattern="[a-z]*"
  vType="text"
  tail="units"
  head={<h4>Text/Icon </h4>}
  helperText="Helper Text"
/>
```
