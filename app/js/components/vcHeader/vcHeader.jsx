import React from 'react';
import { FormattedDate } from 'react-intl';
import Proptypes from 'prop-types';
import cx from 'classnames';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import styles from './vcHeader.scss';

/**
 * Header component with title and date that renders children within itself
 * @param {*} props
 */
const VcHeader = props => (
  <AppBar className={cx(styles.header, props.className)} position="static">
    <Toolbar className={styles.toolbar} disableGutters>
      <Typography className={styles.title} variant="display2" color="inherit">
        {props.title}
      </Typography>
      <div className={styles.buttons}>
        <FormattedDate value={Date.now()} weekday="short" month="2-digit" day="2-digit" />
        {props.children}
      </div>
    </Toolbar>
  </AppBar>
);

VcHeader.propTypes = {
  /** String to be shown as title */
  title: Proptypes.string,
};

VcHeader.defaultProps = {
  title: '',
};

export default VcHeader;
