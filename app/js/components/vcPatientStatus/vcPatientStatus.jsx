import React from 'react';
import { injectIntl } from 'react-intl';
import Proptypes from 'prop-types';
import cx from 'classnames';
import styles from './vcPatientStatus.scss';
import messages from '../../intl/messages';

export const PATIENT_STATUS_ENUM = [
  'waiting',
  'assessment',
  'admitted',
  'postpartum',
  'referral',
  'discharged',
];

/**
 * A component to present the patients status
 * @param {*} props
 */
const VcPatientStatus = (props) => {
  const { formatMessage } = props.intl;

  const statuses = PATIENT_STATUS_ENUM.map((status, index) => (
    <div
      key={status}
      className={cx(styles.statusBubble, {
        [styles.filled]: index <= PATIENT_STATUS_ENUM.indexOf(props.value),
      })}
    />
  ));
  return (
    <div className={styles.patientStatus}>
      <div className={styles.statusLabel}> {formatMessage(messages[props.value])}</div>
      <div className={styles.statusPosition}>
        <div className={styles.throughLine} />
        {statuses}
      </div>
    </div>
  );
};

VcPatientStatus.propTypes = {
  /** string with the status to be displayed */
  value: Proptypes.oneOf(PATIENT_STATUS_ENUM),
};

VcPatientStatus.defaultProps = {
  value: 'waiting',
};

export default injectIntl(VcPatientStatus);
