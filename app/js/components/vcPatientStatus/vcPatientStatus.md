VcPatientStatus example:

```js
<VcGridRow>
  <VcPatientStatus value="waiting" />
  <VcPatientStatus value="postpartum" />
  <VcPatientStatus value="discharged" />
</VcGridRow>
```
