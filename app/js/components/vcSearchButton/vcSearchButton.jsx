import React from 'react';
import { injectIntl } from 'react-intl';
import Proptypes from 'prop-types';
import Avatar from 'material-ui-next/Avatar';
import Grow from 'material-ui-next/transitions/Grow';
import Paper from 'material-ui-next/Paper';
import Input from 'material-ui-next/Input';
import { Manager, Reference, Popper } from 'react-popper';
import Search from 'material-ui-icons-next/Search';
import Clear from 'material-ui-icons-next/Clear';
import styles from './vcSearchButton.scss';
import messages from '../../intl/messages';

/**
 * Search button with pop-up text field to be used in the header
 * @param {*} props
 */
class VcSearchButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }
  // When changing state based on the previous one use a function that gets the previous state and
  // if an action is needed after the state change put that in a callback func as second param
  handleClick = () => {
    this.setState(previousState => ({ open: !previousState.open }), () => {});
  };

  handleClose = () => {
    // setTimeout to ensure a close event comes after a target click event
    this.timeout = setTimeout(() => {
      this.setState({ open: false });
    });
  };

  render() {
    const { formatMessage } = this.props.intl;
    const { open } = this.state;

    return (
      <Manager>
        <Reference>
          {({ ref }) => (
            <div ref={ref}>
              <Avatar onClick={this.handleClick} className={styles.avatar}>
                <Search />
              </Avatar>
            </div>
          )}
        </Reference>
        {open ? (
          <Popper placement="left" eventsEnabled={open}>
            {({
              ref, style, placement, arrowProps,
            }) => (
              <div className={styles.popper} ref={ref} style={style} data-placement={placement}>
                <Grow style={{ transformOrigin: '0 0 0' }} timeout={300} in={open} id="menu-list">
                  <Paper className={styles.paper} elevation={0}>
                    <div className={styles.container}>
                      <Search className={styles.searchIcon} />
                      <Input
                        fullWidth
                        disableUnderline
                        value={this.props.value}
                        placeholder={formatMessage(messages.wardSearchPlaceholder)}
                      />
                      <Clear onClick={this.handleClose} className={styles.icon} />
                    </div>
                  </Paper>
                </Grow>
              </div>
            )}
          </Popper>
        ) : null}
      </Manager>
    );
  }
}

VcSearchButton.propTypes = {
  /** value of the search textfield */
  value: Proptypes.string,
};

VcSearchButton.defaultProps = {
  value: '',
};

export default injectIntl(VcSearchButton, { withRef: true });
