import React from "react";
import Button from "material-ui/Button";
import cx from "classnames";
import { Typography } from "material-ui";
import Proptypes from "prop-types";
import VcButton from "../vcButton/vcButton";
import styles from "./vcSelectOptions.scss";

/**
 * VcSelectOptions is a component using which we can choose an option between various options.
 * If property isMultiSelect is set to true, you can select multiple options.
 * @param {*} props
 */
class VcSelectOptions extends React.Component {
  handleSelected = e => {
    let result = [];
    if (this.props.isMultiSelect) {
      const index = this.props.selectedOptions.indexOf(e);
      result = this.props.selectedOptions;
      if (index === -1) {
        result = result.concat([e]);
      } else {
        result.splice(index, 1);
      }
    } else {
      const index = this.props.selectedOptions.indexOf(e);
      if (index === -1) {
        result = result.concat([e]);
      } else {
        result = this.props.selectedOptions;
        result.splice(index, 1);
      }
    }
    this.props.onSelect(result);
  };

  render() {
    const filters = this.props.filterOptions.map(filter => (
      <div className={styles.vcButton} key={filter}>
        <VcButton
          value={filter}
          type="secondary"
          isSelected={this.props.selectedOptions.indexOf(filter) > -1}
          key={filter}
          onClick={() => this.handleSelected(filter)}
        >
          {filter}
        </VcButton>
      </div>
    ));
    return (
      <div className={styles.container}>
        <div className={styles.text}>
          <Typography variant="caption" gutterBottom>
            {this.props.title}
          </Typography>
        </div>
        {filters}
      </div>
    );
  }
}

VcSelectOptions.propTypes = {
  /** title of component */
  title: Proptypes.string.isRequired,
  /** Array of selected options */
  selectedOptions: Proptypes.array.isRequired,
  /** Array of options */
  filterOptions: Proptypes.array.isRequired,
  /** Call Back function to add or remove the items from selected Array */
  onSelect: Proptypes.func.isRequired,
  /** you can select multiple options when this prop is set to true */
  isMultiSelect: Proptypes.bool
};

VcSelectOptions.defaultProps = {
  isMultiSelect: false,
  onSelect: () => {}
};

export default VcSelectOptions;
