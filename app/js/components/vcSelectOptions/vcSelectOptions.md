VcSelectOptions example:

```js
<VcSelectOptions
  selectedOptions={["Intact"]}
  title="Liquor"
  filterOptions={["Intact", "Clear", "Meconium", "Bloody"]}
/>
```
