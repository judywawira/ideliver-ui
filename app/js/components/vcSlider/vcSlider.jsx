import React from 'react';
import Proptypes from 'prop-types';
import Slider from 'rc-slider';
import TextField from 'material-ui/TextField';
import VcGridRow from '../vcGrid/vcGridRow/vcGridRow';
import styles from './vcSlider.scss';
import '../../../css/theme.scss';
// Imports css globally without modules so that the slider is styled properly
import '!style-loader!css-loader!rc-slider/assets/index.css';

/**
 * A slider component with a text field to enter and display numbers
 * @param {*} props
 */
const VcSlider = props => (
  <VcGridRow className={styles.container}>
    <Slider
      className={styles.slider}
      trackStyle={{ backgroundColor: styles.dark_lavender }}
      handleStyle={{
        borderColor: styles.dark_lavender,
        backgroundColor: styles.dark_lavender,
        height: '20px',
        width: '20px',
        marginTop: '-8px',
      }}
      {...props}
    />
    <TextField
      disabled={props.disabled}
      value={props.value}
      onChange={props.onChange}
      className={styles.textField}
    />
  </VcGridRow>
);
VcSlider.propTypes = {
  /** a boolean to indicate if styling for invalid entry is to be set on the slider */
  invalid: Proptypes.bool,
  /** a boolean to indicate if the slider should be disabled */
  disabled: Proptypes.bool,
  /** value for the slider */
  value: Proptypes.number,
  /** calback function called onChange */
  onChange: Proptypes.func,
};

VcSlider.defaultProps = {
  invalid: false,
  disabled: false,
  value: '',
  onChange: () => {},
};

export default VcSlider;
