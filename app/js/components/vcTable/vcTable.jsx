import React from 'react';
import Proptypes from 'prop-types';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
} from 'material-ui/Table';
import VcTableColumnHeader from '../vcTableColumnHeader/vcTableColumnHeader';
import styles from './vcTable.scss';

/**
 * Table for displaying data used for visits and tasks
 * @param {*} props
 */
const VcTable = (props) => {
  const {
    columnData,
    data,
    order,
    orderBy,
    rowsPerPage,
    page,
    onChangePage,
    onChangeRowsPerPage,
    onSort,
  } = props;

  return (
    <Table className={styles.table}>
      <TableHead>
        <TableRow className={styles.header}>
          {columnData.map(column => (
            <TableCell key={column.id} numeric={column.numeric} padding="none">
              <VcTableColumnHeader
                sortable={column.sortable}
                direction={orderBy === column.id ? order : null}
                filterOptions={column.filterOptions}
                filtersSelected={[1, 3, 0]}
                onSort={onSort}
                numeric={column.numeric}
              >
                {column.label}
              </VcTableColumnHeader>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => (
          <TableRow
            hover
            onClick={event => props.onRowClick(event, n.id)}
            tabIndex={-1}
            key={n.id + n.uuid}
          >
            {columnData.map(column => (
              <TableCell
                key={n.id + column.id}
                padding={column.disablePadding ? 'none' : 'default'}
                numeric={column.numeric}
              >
                {n[column.id]}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
      <TableFooter>
        <TableRow>
          <TablePagination
            colSpan={columnData.length}
            count={data.length}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[10, 20, 30, 40, 50]}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={onChangePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
          />
        </TableRow>
      </TableFooter>
    </Table>
  );
};

VcTable.propTypes = {
  /** array of records to be displayed in the table */
  data: Proptypes.arrayOf(Proptypes.object),
  /**
   * array of column definitions each with keys:
   * id, sortable, filterOptions, numeric, disablePadding, label
   */
  columnData: Proptypes.arrayOf(Proptypes.object).isRequired,
  /** order for the sorted column */
  order: Proptypes.oneOf(['asc', 'desc']),
  /** id of the column that it is sorted by */
  orderBy: Proptypes.string,
  /** number of rows per page */
  rowsPerPage: Proptypes.oneOf([10, 20, 30, 40, 50]),
  /** page number */
  page: Proptypes.number,
  /** callback function for the onChangePage event */
  onChangePage: Proptypes.func,
  /** callback function for the onChangeRowsPerPage event */
  onChangeRowsPerPage: Proptypes.func,
  /** callback function for the onSort event */
  onSort: Proptypes.func,
  /** callback function for the onRowClick event */
  onRowClick: Proptypes.func,
};

VcTable.defaultProps = {
  data: [],
  page: 0,
  rowsPerPage: 10,
  onChangePage: () => {},
  onRowClick: () => {},
};

export default VcTable;
