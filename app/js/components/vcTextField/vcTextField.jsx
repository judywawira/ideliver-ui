import React from "react";
import Proptypes from "prop-types";
import TextField from "material-ui/TextField";
import { FormControl, FormHelperText } from "material-ui/Form";

/**
 * Button component to be used for all standard buttons
 * @param {*} props
 */
class VcTextField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: this.props.value
    };
  }

  handleChange = event => {
    this.setState({ comment: event.target.value }, function() {
      this.props.onTextChange(this.state.comment);
    });
  };

  render() {
    return (
      <div>
        <FormControl fullWidth>
          <TextField
            type="text"
            label={this.props.label}
            value={this.state.comment}
            onChange={this.handleChange}
            inputProps={{
              maxLength: 200
            }}
          />
        </FormControl>
      </div>
    );
  }
}

VcTextField.propTypes = {
  /** value to be displayed in the text field */
  value: Proptypes.string,
  /** label to be displayed in the text field */
  label: Proptypes.string,
  /** max number of characters allowed in the text field area */
  maxLength: Proptypes.number,
  /** Call Back function when value changes */
  onTextChange: Proptypes.func.isRequired
};

VcTextField.defaultProps = {
  value: "",
  maxLength: 200,
  label: "Add comment",
  onTextChange: () => {}
};

export default VcTextField;
