import React from 'react';
import Proptypes from 'prop-types';
import cx from 'classnames';
import styles from './vcToggle.scss';

/**
 * A component for switching true/false
 * @param {*} props
 */
const VcToggle = props => (
  <label htmlFor="toggle" className={styles.sectionToggle}>
    <input
      type="checkbox"
      id="toggle"
      checked={props.value}
      onChange={() => props.onChange(!props.value)}
    />
    <div className={styles.slider}>
      <span className={styles.toggleCheckboxSlider} />
      <div className={cx(styles.sliderLabel, styles.no)}>No</div>
      <div className={cx(styles.sliderLabel, styles.yes)}>Yes</div>
    </div>
  </label>
);

VcToggle.propTypes = {
  /** a boolean to indicate if styling for invalid entry is to be set on the toggle button */
  invalid: Proptypes.bool,
  /** a boolean to indicate if the slider should be disabled */
  disabled: Proptypes.bool,
  /** value for the toggle button */
  value: Proptypes.bool,
  /** callback function to be fired onChange */
  onChange: Proptypes.func,
};

VcToggle.defaultProps = {
  invalid: false,
  disabled: false,
  value: '',
  onChange: () => {},
};

export default VcToggle;
