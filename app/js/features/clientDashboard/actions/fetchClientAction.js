export const FETCH_CLIENT_ACTION = 'FETCH_CLIENT_ACTION';

const fetchClientAction = (url, visitId) => ({
  type: FETCH_CLIENT_ACTION,
  url,
  visitId,
});

export default fetchClientAction;
