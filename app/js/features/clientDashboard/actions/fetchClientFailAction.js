export const FETCH_CLIENT_FAILED = 'FETCH_CLIENT_FAILED';

const fetchClientFailAction = value => ({
  type: FETCH_CLIENT_FAILED,
  payload: value,
});

export default fetchClientFailAction;
