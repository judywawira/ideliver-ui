export const FETCH_CLIENT_SUCCEEDED = 'FETCH_CLIENT_SUCCEEDED';

const fetchClientSuccessAction = value => ({
  type: FETCH_CLIENT_SUCCEEDED,
  payload: value,
});

export default fetchClientSuccessAction;
