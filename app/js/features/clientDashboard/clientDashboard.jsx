import React from 'react';
import { injectIntl } from 'react-intl';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import VcHeader from '../../components/vcHeader/vcHeader';
import VcCardWithDialog from '../../components/vcCardWithDialog/vcCardWithDialog';
import VcGrid from '../../components/vcGrid/vcGrid';
import VcGridRow from '../../components/vcGrid/vcGridRow/vcGridRow';
import ClientSidebar from '../../features/clientSidebar/clientSidebar';
import VcClientCard from '../../components/vcClientCard/vcClientCard';
import styles from './clientDashboard.scss';
import messages from '../../intl/messages';

class ClientDashboard extends React.Component {
  componentDidMount() {
    this.props.dispatchFetchClientAction();
  }

  render() {
    const { formatMessage } = this.props.intl;
    const { person } = this.props.visit ? this.props.visit.patient : { person: {} };
    return [
      <ClientSidebar location={this.props.location} key="clientSidebar" />,
      <VcHeader className={styles.header} key="clientHeader" />,
      <VcGrid className={styles.container} key="grid">
        <VcGridRow className={styles.firstRow}>
          <VcClientCard age={person.age} className={styles.patientCard} />
        </VcGridRow>
        <VcGridRow className={styles.row}>
          <div className={styles.col}>
            <VcCardWithDialog
              title={formatMessage(messages.cardHeaderPossibleDiagnosis)}
              value="Possible Diagnosis"
              filterOptions={[]}
            />
          </div>
          <div className={styles.col}>
            <VcCardWithDialog
              title={formatMessage(messages.cardHeaderDiagnosis)}
              value="Diagnosis"
              filterOptions={[]}
            />
          </div>
          <div className={styles.col}>
            <VcCardWithDialog
              title={formatMessage(messages.cardHeaderSummary)}
              value="Summary"
              filterOptions={[]}
            />
          </div>
        </VcGridRow>
        {this.props.alerts && this.props.alerts.length > 0 ? (
          <VcGridRow className={styles.row}>
            <VcAlertsCard alerts={this.props.alerts} />
          </VcGridRow>
        ) : null}
        <VcGridRow className={styles.row}>
          <Route
            path="/client/:id/vitals"
            component={() => <div>PUT THE VITALS PAGE HERE</div>}
          />
          <Route
            path="/client/:id/assessment"
            component={() => <div>PUT THE ASSESSMENT PAGE HERE</div>}
          />
          <Route
            path="/client/:id/activeLabour"
            component={() => <div>PUT THE ACTIVE LABOUR PAGE HERE</div>}
          />
          <Route
            path="/client/:id/orders"
            component={() => <div>PUT THE ORDERS PAGE HERE</div>}
          />
          <Route
            path="/client/:id/tasks"
            component={() => <div>PUT THE TASKS PAGE HERE</div>}
          />
          <Route
            path="/client/:id/adt"
            component={() => <div>PUT THE ADT PAGE HERE</div>}
          />
          <Route
            path="/client/:id/patientInfo"
            component={() => <div>PUT THE PATIENT INFO PAGE HERE</div>}
          />
        </VcGridRow>
      </VcGrid>
    ];
  }
}

ClientDashboard.propTypes = {
  // array of alerts to be displayed
  alerts: PropTypes.array,
  // callback function to be fired on component did mount
  dispatchFetchClientAction: PropTypes.func,
};

ClientDashboard.defaultProps = {};

export default injectIntl(ClientDashboard);
