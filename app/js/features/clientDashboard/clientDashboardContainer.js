import { connect } from 'react-redux';
import ClientDashboard from './clientDashboard';
import { REST_API_PATHNAME } from '../../paths';
import fetchClientAction from './actions/fetchClientAction';

const mapStateToProps = (state, props) => {
  const visitId = props.location.pathname.substring(8, 44);
  return {
    visit: state.Visits.details.toJSON()[visitId],
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const visitId = props.location.pathname.substring(8, 44);
  return {
    dispatchFetchClientAction: () => {
      dispatch(fetchClientAction(REST_API_PATHNAME, visitId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientDashboard);
