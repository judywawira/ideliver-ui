import 'regenerator-runtime/runtime';
import { call, put } from 'redux-saga/effects';
import fetchClientSuccessAction from '../actions/fetchClientSuccessAction';
import fetchClientFailAction from '../actions/fetchClientFailAction';
import { fetchUrl } from '../../../api';

// worker Saga: will be fired on FETCH_CLIENT_ACTION actions
export default function* fetchClient(action) {
  try {
    let client = yield call(
      fetchUrl,
      `${action.url}visit/${action.visitId}?v=custom:(uuid,patient:(person))`,
    );
    client = { [client.uuid]: client };
    yield put(fetchClientSuccessAction(client));
  } catch (e) {
    yield put(fetchClientFailAction(e.message));
  }
}
