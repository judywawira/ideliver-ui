import React from 'react';
import { injectIntl } from 'react-intl';
import Proptypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import KeyboardBackspace from 'material-ui-icons-next/KeyboardBackspace';
import VcDrawer from '../../components/vcDrawer/vcDrawer';
import styles from './clientSidebar.scss';
import VcSidebarButton from '../../components/vcSidebarButton/vcSidebarButton';
import messages from '../../intl/messages';

/**
 * Sidebar component with routes to different parts of the app
 * @param {*} props
 */
const ClientSidebar = (props) => {
  const { formatMessage } = props.intl;
  const clientId = props.location.pathname.substring(8, 44);

  return (
    <VcDrawer open anchor="left" variant="persistent">
      <NavLink className={styles.navLink} to="/visits">
        <VcSidebarButton icon={<KeyboardBackspace />} />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/vitals`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/vitals`}
          label={formatMessage(messages.vitals)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/assessment`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/assessment`}
          label={formatMessage(messages.assessment)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/activeLabour`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/activeLabour`}
          label={formatMessage(messages.activeLabour)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/orders`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/orders`}
          label={formatMessage(messages.orders)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/tasks`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/tasks`}
          label={formatMessage(messages.tasks)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/adt`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/adt`}
          label={formatMessage(messages.adt)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to={`/client/${clientId}/patientInfo`}>
        <VcSidebarButton
          selected={props.location === `/client/${clientId}/patientInfo`}
          label={formatMessage(messages.patientInfo)}
        />
      </NavLink>
    </VcDrawer>
  );
};

ClientSidebar.propTypes = {
  /** string that if matched with the route on a button sets its prop selected to true */
  location: Proptypes.object,
  /** string with the id of the client */
  clientId: Proptypes.string,
};

ClientSidebar.defaultProps = {};

export default injectIntl(ClientSidebar);
