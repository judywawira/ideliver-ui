export const DEMO_ACTION = 'DEMO_ACTION';

const demoAction = value => ({
  type: DEMO_ACTION,
  payload: value,
});

export default demoAction;
