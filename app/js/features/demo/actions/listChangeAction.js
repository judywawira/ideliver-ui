export const LIST_CHANGE_ACTION = "LIST_CHANGE_ACTION";

const listChangeAction = value => {
  return {
    type: LIST_CHANGE_ACTION,
    payload: value
  };
};

export default listChangeAction;
