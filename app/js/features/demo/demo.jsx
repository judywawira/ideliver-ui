import React from 'react';
import PropTypes from 'prop-types';
import VcButton from '../../components/vcButton/vcButton';
import VcToggle from '../../components/vcToggle/vcToggle';
import WardHeader from '../wardHeader/wardHeader';
import VcSelectOptions from '../../components/vcSelectOptions/vcSelectOptions';
import VcGridRow from '../../components/vcGrid/vcGridRow/vcGridRow';
import VcNumericDropDown from '../../components/vcNumericDropDown/vcNumericDropDown';
import MainSidebar from '../../features/mainSidebar/mainSidebar';
import VcCardWithDialog from '../../components/vcCardWithDialog/vcCardWithDialog';

class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    };
  }

  handleChange = (e) => {
    this.setState({ textField: e.target.value });
  };

  handleFiltersChange = (index) => {
    this.setState((prevState) => {
      prevState.filtersSelected.indexOf(index) === -1
        ? prevState.filtersSelected.push(index)
        : prevState.filtersSelected.splice(prevState.filtersSelected.indexOf(index), 1);
      return { filtersSelected: prevState.filtersSelected };
    });
  };

  handleSummaryChange = (value, index) => {
    this.setState((prevState) => {
      if (value) {
        prevState.filterComments[index] = value;
      } else {
        prevState.filterComments.splice(index, 1);
      }
      return { filterComments: prevState.filterComments };
    });
  };

  handleToggle = (e) => {
    this.setState({ toggle: e });
  };

  handleClick = () => {
    this.props.dispatchDemoAction(this.state.textField);
  };

  handleListChange = () => {
    this.props.dispatchListChangeAction();
  };

  handleSelectOptions = (e) => {
    this.setState({
      selectedOptions: e,
    });
  };
  handleNumericDropDownChange = (e) => {
    this.setState({ value: e });
  };

  render() {
    return [
      <MainSidebar location={this.props.location.pathname} key="mainSidebar" />,
      <WardHeader key="wardHeader" />,
      <div className="demo" key="demo">
        <h1>Hello, {this.props.name}</h1>
        <br />
        <br />
        <button id="button1" onClick={this.handleClick} type="button">
          Change Greeting
        </button>
        <input type="text" onChange={this.handleChange} value={this.state.textField} />
        <br />
        <button id="button2" onClick={this.handleListChange} type="button">
          Do something to the list
        </button>
        <VcButton value={this.props.name} />
        <VcToggle value={this.state.toggle} onChange={this.handleToggle} />
        <VcSelectOptions
          title="Liquor"
          filterOptions={['Intact', 'Clear', 'Meconium', 'Bloody']}
          selectedOptions={this.state.selectedOptions}
          isMultiSelect
          onSelect={this.handleSelectOptions}
        />
        <VcSelectOptions
          title="Moulding"
          filterOptions={['0', '+', '++', '+++']}
          selectedOptions={this.state.selectedOptions}
          onSelect={this.handleSelectOptions}
        />
        <VcNumericDropDown
          value={this.state.value}
          title="urineVolume"
          onChange={this.handleNumericDropDownChange}
          stepSize={20}
          start={50}
          end={220}
        />
      </div>,
      <VcGridRow>
        <VcCardWithDialog
          key="Visit Summary"
          title="Visit Summary"
          value="Summary"
          onAddComment={this.handleSummaryChange}
          filterComments={this.state.filterComments}
        />
        <VcCardWithDialog
          key="Possible diagnosis"
          title="Possible diagnosis"
          value="Possible Diagnosis"
          filterOptions={this.state.filterOptions}
          onFiltersSelected={this.handleFiltersChange}
          filtersSelected={this.state.filtersSelected}
        />
        <VcCardWithDialog
          key="Diagnosis"
          title="Diagnosis"
          value="Diagnosis"
          filterOptions={this.state.filterOptions}
          onFiltersSelected={this.handleFiltersChange}
          filtersSelected={this.state.filtersSelected}
        />
      </VcGridRow>,
    ];
  }
}

Demo.propTypes = {
  // the name to be greeted
  name: PropTypes.string,
  // callback function to fire on dispatchDemoAction
  dispatchDemoAction: PropTypes.func.isRequired,
  // callback function to fire on dispatchListChangeAction
  dispatchListChangeAction: PropTypes.func.isRequired,
};

Demo.defaultProps = {
  name: 'Earth',
};

export default Demo;
