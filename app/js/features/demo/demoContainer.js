import { connect } from 'react-redux';
import Demo from './demo';
import demoAction from './actions/demoAction';
import listChangeAction from './actions/listChangeAction';

const mapStateToProps = store => ({
  name: store.Demo.name,
});

const mapDispatchToProps = dispatch => ({
  dispatchDemoAction: (text) => {
    dispatch(demoAction(text));
  },
  dispatchListChangeAction: () => {
    dispatch(listChangeAction(['a', 'b', 'c', 'd', 'e', 'f', 'g']));
  },
});

const DemoContainer = connect(mapStateToProps, mapDispatchToProps)(Demo);

export default DemoContainer;
