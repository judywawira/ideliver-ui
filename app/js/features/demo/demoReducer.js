import { fromJS } from 'immutable';
import initialState from '../../initialState';
import { DEMO_ACTION } from './actions/demoAction';
import { LIST_CHANGE_ACTION } from './actions/listChangeAction';

const demoReducer = (state = initialState, action) => {
  switch (action.type) {
  case DEMO_ACTION:
    return Object.assign({}, state, {
      name: action.payload,
    });
  case LIST_CHANGE_ACTION:
    state.listOfSomething = fromJS(action.payload);
    return state;
  default:
    return state;
  }
};

export default demoReducer;
