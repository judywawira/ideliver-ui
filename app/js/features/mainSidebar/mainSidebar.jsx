import React from 'react';
import { injectIntl } from 'react-intl';
import Proptypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import VcDrawer from '../../components/vcDrawer/vcDrawer';
import styles from './mainSidebar.scss';
import VcSidebarButton from '../../components/vcSidebarButton/vcSidebarButton';
import messages from '../../intl/messages';

/**
 * Sidebar component with routes to different parts of the app
 * @param {*} props
 */
const MainSidebar = (props) => {
  const { formatMessage } = props.intl;

  return (
    <VcDrawer open anchor="left" variant="persistent">
      <NavLink to="/">
        <VcSidebarButton icon={<img className={styles.icon} src="img/l-o-g-o@3x.png" />} />
      </NavLink>
      <NavLink className={styles.navLink} to="/visits">
        <VcSidebarButton
          selected={props.location === '/visits'}
          icon={<img className={styles.icon} src="img/team-icon@3x.png" />}
          iconSelected={<img className={styles.icon} src="img/clients-icon@3x.png" />}
          label={formatMessage(messages.clients)}
        />
      </NavLink>
      <NavLink className={styles.navLink} to="/tasks">
        <VcSidebarButton
          selected={props.location === '/tasks'}
          icon={<img className={styles.icon} src="img/task-icon@3x.png" />}
          iconSelected={<img className={styles.icon} src="img/task-icon-white@3x.png" />}
          label={formatMessage(messages.tasks)}
        />
      </NavLink>
      <VcSidebarButton
        label={formatMessage(messages.team)}
        icon={<img className={styles.icon} src="img/team-icon@3x.png" />}
        iconSelected={<img className={styles.icon} src="img/clients-icon@3x.png" />}
      />
      <img className={styles.clientIcon} src="img/client-summary-placeholder.png" />
    </VcDrawer>
  );
};

MainSidebar.propTypes = {
  /** string that if matched with the route on a button sets its prop selected to true */
  location: Proptypes.string,
};

MainSidebar.defaultProps = {};

export default injectIntl(MainSidebar);
