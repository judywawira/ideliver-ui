export const FETCH_VISITS_ACTION = 'FETCH_VISITS_ACTION';

const fetchVisitsAction = (url, pageSize, pageIndex) => ({
  type: FETCH_VISITS_ACTION,
  url,
  pageSize,
  pageIndex,
});

export default fetchVisitsAction;
