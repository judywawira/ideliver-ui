import 'regenerator-runtime/runtime';
import React from 'react';
import { FormattedDate } from 'react-intl';
import { call, put } from 'redux-saga/effects';
import fetchVisitsSuccessAction from '../actions/fetchVisitsSuccessAction';
import fetchVisitsFailAction from '../actions/fetchVisitsFailAction';
import { fetchUrl } from '../../../api';

// worker Saga: will be fired on FETCH_VISITS_ACTION actions
export default function* fetchVisits(action) {
  try {
    const visitsTotal = yield call(
      fetchUrl,
      `${action.url}visit/?v=custom:(uuid,startDatetime,patient:(uuid,display))`,
    );
    for (let i = 0; i < visitsTotal.results.length; i++) {
      const visit = visitsTotal.results[i];
      const { patient } = visit;
      const idName = patient.display.split(' ');
      visitsTotal.results[i] = {
        id: visit.uuid,
        mrn: idName[0],
        givenName: idName[2],
        familyName: idName[4] ? idName[4] : idName[3],
        admissionTime: (
          <FormattedDate
            value={visit.startDatetime}
            month="long"
            day="2-digit"
            hour="2-digit"
            minute="2-digit"
            second="2-digit"
          />
        ),
      };
    }
    yield put(fetchVisitsSuccessAction(visitsTotal.results));
  } catch (e) {
    yield put(fetchVisitsFailAction(e.message));
  }
}
