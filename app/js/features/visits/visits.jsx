import React from 'react';
import { withRouter } from 'react-router-dom';
import { LinearProgress } from 'material-ui/Progress';
import WardHeader from '../wardHeader/wardHeader';
import styles from './visits.scss';
import VcTable from '../../components/vcTable/vcTable';
import MainSidebar from '../../features/mainSidebar/mainSidebar';
import { APP_PATHNAME } from '../../paths';

class Visits extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageSize: 10,
      pageIndex: 0,
    };
  }

  componentDidMount() {
    this.props.dispatchFetchVisitsAction(this.state.pageSize, this.state.pageIndex);
  }

  handleChangePage = (...args) => {
    this.props.dispatchFetchVisitsAction(this.state.pageSize, args[1]);
    this.setState({ pageIndex: args[1] });
  };

  handleRowsChange = (...args) => {
    this.props.dispatchFetchVisitsAction(args[0].target.value, this.state.pageIndex);
    this.setState({ pageSize: args[0].target.value });
  };

  render() {
    const columnData = [
      {
        id: 'givenName',
        sortable: true,
        numeric: false,
        disablePadding: false,
        label: 'First Name',
      },
      {
        id: 'familyName',
        sortable: true,
        numeric: false,
        disablePadding: false,
        label: 'Last Name',
      },
      {
        id: 'mrn',
        sortable: true,
        numeric: true,
        disablePadding: false,
        label: 'MRN',
      },
      {
        id: 'status',
        sortable: false,
        filterOptions: ['Waiting', 'Assessment', 'Admitted', 'Postpartum', 'Referral'],
        numeric: false,
        disablePadding: false,
        label: 'Status',
      },
      {
        id: 'acuity',
        sortable: true,
        filterOptions: ['1', '2', '3', '4', '5'],
        numeric: true,
        disablePadding: false,
        label: 'Acuity',
      },
      {
        id: 'admissionTime',
        sortable: true,
        numeric: true,
        disablePadding: false,
        label: 'Admission Time',
      },
      {
        id: 'diagnosis',
        sortable: false,
        numeric: false,
        disablePadding: false,
        label: 'Diagnosis',
      },
      {
        id: 'alerts',
        sortable: false,
        numeric: false,
        disablePadding: false,
        label: 'Alerts',
      },
    ];

    const progressBar =
      this.props.value.length > 0 &&
      this.props.value[this.state.pageSize * this.state.pageIndex] &&
      this.props.value[this.state.pageSize * this.state.pageIndex].id ? null : (
        <LinearProgress />
        );

    return [
      <MainSidebar location={this.props.location.pathname} key="mainSidebar" />,
      <div key="visits">
        <WardHeader key="wardHeader" />
        {progressBar}
        <div key="body" className={styles.container}>
          <VcTable
            onRowClick={(event, id) => {
              this.props.history.push(`/client/${id}`);
            }}
            page={this.state.pageIndex}
            rowsPerPage={this.state.pageSize}
            columnData={columnData}
            data={this.props.value}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleRowsChange}
          />
        </div>
      </div>,
    ];
  }
}

export default withRouter(Visits);
