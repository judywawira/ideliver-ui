import { connect } from 'react-redux';
import Visits from './visits';
import fetchVisitsAction from './actions/fetchVisitsAction';
import { REST_API_PATHNAME } from '../../paths';

const mapStateToProps = store => ({
  value: store.Visits.list.toJSON(),
});

const mapDispatchToProps = dispatch => ({
  dispatchFetchVisitsAction: (pageSize, pageIndex) => {
    dispatch(fetchVisitsAction(REST_API_PATHNAME, pageSize, pageIndex));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Visits);
