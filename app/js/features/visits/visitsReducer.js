import { fromJS, List, Map } from 'immutable';
import { FETCH_VISITS_SUCCEEDED } from './actions/fetchVisitsSuccessAction';
import { FETCH_CLIENT_SUCCEEDED } from '../clientDashboard/actions/fetchClientSuccessAction';
import { FETCH_VISITS_FAILED } from './actions/fetchVisitsFailAction';

const visitsReducer = (
  state = { list: new List(), validation: new List(), details: new Map() },
  action,
) => {
  switch (action.type) {
  case FETCH_VISITS_SUCCEEDED:
    return Object.assign({}, state, { list: fromJS(action.payload) });

  case FETCH_CLIENT_SUCCEEDED:
    return Object.assign({}, state, { details: fromJS(action.payload) });

  case FETCH_VISITS_FAILED:
    return Object.assign({}, state, {
      validation: fromJS(action.payload),
    });

  default:
    return state;
  }
};

export default visitsReducer;
