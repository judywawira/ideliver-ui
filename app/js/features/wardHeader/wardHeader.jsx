import React from 'react';
import { injectIntl } from 'react-intl';
import VcHeader from '../../components/vcHeader/vcHeader';
import messages from '../../intl/messages';
import VcSearchButton from '../../components/vcSearchButton/vcSearchButton';
import VcAlertsButton from '../../components/vcAlertsButton/vcAlertsButton';

const WardHeader = (props) => {
  const { formatMessage } = props.intl;

  return (
    <VcHeader title={formatMessage(messages.wardHeader)}>
      <VcAlertsButton />
      <VcSearchButton />
    </VcHeader>
  );
};

WardHeader.propTypes = {};

WardHeader.defaultProps = {};

export default injectIntl(WardHeader);
