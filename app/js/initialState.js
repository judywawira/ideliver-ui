import { List } from 'immutable';

const initialState = {
  name: 'Mars',
  listOfSomething: List.of(1, 2, 3, 4, 5, 6, 7),
  data: new List(),
  error: '',
};

export default initialState;
