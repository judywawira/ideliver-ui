import { defineMessages } from "react-intl";

const Messages = defineMessages({
  clients: {
    id: "clients",
    defaultMessage: "Clients"
  },
  tasks: {
    id: "tasks",
    defaultMessage: "Tasks"
  },
  team: {
    id: "team",
    defaultMessage: "Team"
  },
  wardHeader: {
    id: "wardHeader",
    defaultMessage: "Ward Dashboard"
  },
  wardAlerts: {
    id: "wardAlerts",
    defaultMessage: "Ward Alerts"
  },
  inlineAlerts: {
    id: "inlineAlerts",
    defaultMessage: "Alerts"
  },
  wardSearchPlaceholder: {
    id: "searchPlaceholder",
    defaultMessage: "Search by Client Last Name or MRN"
  },
  selectAll: {
    id: "selectAll",
    defaultMessage: "Select All"
  },
  acuity: {
    id: "acuity",
    defaultMessage: "Acuity"
  },
  nextCheckUp: {
    id: "nextCheckUp",
    defaultMessage: "Next check-up"
  },
  overdue: {
    id: "overdue",
    defaultMessage: "Overdue"
  },
  waiting: {
    id: "waiting",
    defaultMessage: "Waiting"
  },
  assessment: {
    id: "assessment",
    defaultMessage: "Assessment"
  },
  admitted: {
    id: "admitted",
    defaultMessage: "Admitted"
  },
  postpartum: {
    id: "postpartum",
    defaultMessage: "Postpartum"
  },
  referral: {
    id: "referral",
    defaultMessage: "Referral"
  },
  discharged: {
    id: "discharged",
    defaultMessage: "Discharged"
  },
  week: {
    id: "week",
    defaultMessage: "Week"
  },
  age: {
    id: "age",
    defaultMessage: "Age"
  },
  companion: {
    id: "companion",
    defaultMessage: "Companion"
  },
  education: {
    id: "education",
    defaultMessage: "Education"
  },
  ancRisk: {
    id: "ancRisk",
    defaultMessage: "ANC Risk factors"
  },
  presentingComplaints: {
    id: "presentingComplaints",
    defaultMessage: "Presenting Complaints"
  },
  task: {
    id: "task",
    defaultMessage: "TASK"
  },
  gravidityAbrev: {
    id: "gravidityAbrev",
    defaultMessage: "G"
  },
  termBirthsAbrev: {
    id: "termBirthsAbrev",
    defaultMessage: "T"
  },
  pretermBirthsAbrev: {
    id: "pretermBirthsAbrev",
    defaultMessage: "P"
  },
  abortionsAbrev: {
    id: "abortionsAbrev",
    defaultMessage: "A"
  },
  livingChildrenAbrev: {
    id: "livingChildrenAbrev",
    defaultMessage: "L"
  },
  yes: {
    id: "yes",
    defaultMessage: "Yes"
  },
  no: {
    id: "no",
    defaultMessage: "No"
  },
  regular: {
    id: "regular",
    defaultMessage: "Regular"
  },
  irregular: {
    id: "irregular",
    defaultMessage: "Irregular"
  },
  alert: {
    id: "alert",
    defaultMessage: "Alert"
  },
  alertIrregular: {
    id: "alertIrregular",
    defaultMessage: "Alert and Irregular"
  },
  moulding: {
    id: "moulding",
    defaultMessage: "Moulding"
  },
  liquor: {
    id: "liquor",
    defaultMessage: "Liquor"
  },
  normal: {
    id: "normal",
    defaultMessage: "Normal"
  },
  addReading: {
    id: "addReading",
    defaultMessage: "Add Reading"
  },
  cardHeaderDiagnosis: {
    id: "cardHeaderDiagnosis",
    defaultMessage: "Diagnosis"
  },
  cardHeaderPossibleDiagnosis: {
    id: "cardHeaderPossibleDiagnosis",
    defaultMessage: "Possible diagnosis"
  },
  cardHeaderSummary: {
    id: "cardHeaderSummary",
    defaultMessage: "Visit summary"
  },
  cardContentDiagnosis: {
    id: "content",
    defaultMessage: "No diagnosis assigned."
  },
  cardContentPossibleDiagnosis: {
    id: "cardContentPossibleDiagnosis",
    defaultMessage: "No possible diagnosis assigned."
  },
  cardContentSummary: {
    id: "cardContentSummary",
    defaultMessage: "Summary missing"
  },
  dialogHeaderDiagnosis: {
    id: "dialogHeaderDiagnosis",
    defaultMessage: "Add diagnosis"
  },
  dialogHeaderPossibleDiagnosis: {
    id: "dialogHeaderPossibleDiagnosis",
    defaultMessage: "Add possible diagnosis"
  },
  dialogHeaderSummary: {
    id: "dialogHeaderSummary",
    defaultMessage: "Add visit summary"
  },
  heartRate: {
    id: "heartRate",
    defaultMessage: "Heart rate (bpm)"
  },
  diastolic: {
    id: "diastolic",
    defaultMessage: "Diastolic"
  },
  temperature: {
    id: "temperature",
    defaultMessage: "Temperature(°C)"
  },
  urineProtein: {
    id: "urineProtein",
    defaultMessage: "Urine Protein"
  },
  urineAcetone: {
    id: "urineAcetone",
    defaultMessage: "Urine Acetone"
  },
  urineVolume: {
    id: "urineVolume",
    defaultMessage: "Urine Volume"
  },
  vitals: {
    id: "vitals",
    defaultMessage: "Vitals"
  },
  activeLabour: {
    id: "activeLabour",
    defaultMessage: "Active Labour"
  },
  orders: {
    id: "orders",
    defaultMessage: "Orders"
  },
  adt: {
    id: "adt",
    defaultMessage: "ADT"
  },
  patientInfo: {
    id: "patientInfo",
    defaultMessage: "Patient Information"
  },
  toggleYes: {
    id: "toggleYes",
    defaultMessage: "Yes"
  },
  toggleNo: {
    id: "toggleNo",
    defaultMessage: "No"
  }
});

export default Messages;
