import { combineReducers } from 'redux';

import demoReducer from './features/demo/demoReducer';
import visitsReducer from './features/visits/visitsReducer';

// Use ES6 object literal shorthand syntax to define the object shape
const rootReducer = combineReducers({
  Demo: demoReducer,
  Visits: visitsReducer,
});

export default rootReducer;
