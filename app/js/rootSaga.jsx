import 'regenerator-runtime/runtime';
import { takeLatest } from 'redux-saga/effects';
import { FETCH_VISITS_ACTION } from './features/visits/actions/fetchVisitsAction';
import { FETCH_CLIENT_ACTION } from './features/clientDashboard/actions/fetchClientAction';
import fetchVisits from './features/visits/sagas/fetchVisitsSaga';
import fetchClient from './features/clientDashboard/sagas/fetchClientSaga';

function* rootSaga() {
  yield [
    takeLatest(FETCH_VISITS_ACTION, fetchVisits),
    takeLatest(FETCH_CLIENT_ACTION, fetchClient),
  ];
}

export default rootSaga;
