import { createMuiTheme } from 'material-ui/styles';
import deepPurple from 'material-ui/colors/deepPurple';
import lightGreen from 'material-ui/colors/lightGreen';

const theme = createMuiTheme({
  palette: {
    primary: deepPurple,
    textSecondary: lightGreen,
  },
  typography: {
    display2: {
      fontSize: '40px',
    },
  },
});

export default theme;
