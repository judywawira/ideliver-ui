import { fetchUrl } from '../js/api';

describe('api', () => {
  beforeEach(() => {
    global.fetch = jest.fn().mockImplementation(() => {
      const p = new Promise((resolve, reject) => {
        resolve({
          ok: true,
          Id: '123',
          json() {
            return { Id: '123' };
          },
        });
      });

      return p;
    });
  });

  it('fetchUrl', async () => {
    const response = await fetchUrl('foo');
    expect(response.Id).toBe('123');
  });
});
