import React from 'react';
import { mountWithIntl, shallowWithIntl } from '../../enzyme-test-helpers';
import VcCardWithDialog from '../../../js/components/vcCardWithDialog/vcCardWithDialog';

describe('VcCardWithDialog', () => {
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });

  it('renders without crashing', () => {
    const component = mountWithIntl(<VcCardWithDialog
      value="Diagnosis"
      title="Diagnosis"
      filterOptions={['Intact', 'Clear', 'Meconium', 'Bloody']}
    />);
    expect(component).toBeDefined();
  });

  it('renders without crashing', () => {
    const component = mountWithIntl(<VcCardWithDialog value="Summary" title="Visit Summary" />);
    expect(component).toBeDefined();
  });

  it('renders without crashing', () => {
    const component = mountWithIntl(<VcCardWithDialog value="Summary" title="Visit Summary" />);
    expect(component).toBeDefined();
  });

  it('toggles state expanded when cross circle image clicked', () => {
    const component = mountWithIntl(shallowWithIntl(<VcCardWithDialog
      value="Diagnosis"
      title="Diagnosis"
      filterOptions={['Intact', 'Clear', 'Meconium', 'Bloody']}
    />).get(0));

    const button = component.find('IconButton').at(0);

    button.simulate('click');
    expect(component.state()).toEqual({
      comment: '',
      expanded: true,
      index: 0,
      open: true,
    });
    button.simulate('click');
    expect(component.state()).toEqual({
      comment: '',
      expanded: false,
      index: 0,
      open: true,
    });
  });

  it('checks functionality when adding a comment ', () => {
    const component = mountWithIntl(shallowWithIntl(<VcCardWithDialog
      value="Summary"
      title="Visit Summary"
      onAddComment={mockFunc}
      filterComments={[]}
    />).get(0));

    const button = component.find('IconButton').at(0);

    button.simulate('click');

    const buttonSubmit = component
      .find('VcDialogContentSummary')
      .find('VcButton')
      .at(0);

    const VcTextField = component
      .find('VcDialogContentSummary')
      .find('VcTextField')
      .getElement();

    VcTextField.props.onTextChange('nest');

    buttonSubmit.simulate('click');
    expect(mockFunc.mock.calls.length).toBe(1);
    expect(component.state()).toEqual({
      comment: '',
      expanded: true,
      index: 0,
      open: true,
    });
  });

  it('checks functionality when editing a comment ', () => {
    const component = mountWithIntl(shallowWithIntl(<VcCardWithDialog
      onAddComment={mockFunc}
      value="Summary"
      title="Visit Summary"
      filterComments={['Test']}
    />).get(0));

    const button = component
      .find('VcDiagnosisCardContent')
      .find('IconButton')
      .at(0);

    button.simulate('click');
    expect(component.state()).toEqual({
      comment: 'Test',
      expanded: true,
      index: 0,
      open: true,
    });

    const buttonSubmit = component
      .find('VcDialogContentSummary')
      .find('VcButton')
      .at(0);

    const VcTextField = component
      .find('VcDialogContentSummary')
      .find('VcTextField')
      .getElement();

    VcTextField.props.onTextChange('nest');

    buttonSubmit.simulate('click');
    expect(mockFunc.mock.calls.length).toBe(1);
    expect(component.state()).toEqual({
      comment: 'Test',
      expanded: true,
      index: 0,
      open: true,
    });
  });

  it('checks functionality when selecting a checkbox from list of diagnosis ', () => {
    const component = mountWithIntl(shallowWithIntl(<VcCardWithDialog
      value="Diagnosis"
      title="Diagnosis"
      filterOptions={['Intact', 'Clear', 'Meconium', 'Bloody']}
    />).get(0));

    const button = component.find('IconButton').at(0);

    button.simulate('click');

    const checkbox = component.find('input[type="checkbox"]').at(0);

    checkbox.simulate('change');

    expect(component.state()).toEqual({
      comment: '',
      expanded: true,
      index: 0,
      open: true,
    });

    checkbox.simulate('change');
    expect(component.state()).toEqual({
      comment: '',
      expanded: true,
      index: 0,
      open: true,
    });
  });
  it('checks functionality when close button in dialog header is clicked ', () => {
    const component = mountWithIntl(shallowWithIntl(<VcCardWithDialog
      value="Possible Diagnosis"
      title="Possible Diagnosis"
      filterOptions={['Intact', 'Clear', 'Meconium', 'Bloody']}
    />).get(0));

    const button = component.find('IconButton').at(0);

    button.simulate('click');

    const buttonClose = component
      .find('VcDialog')
      .find('button')
      .at(0);

    buttonClose.simulate('click');

    expect(component.state()).toEqual({
      comment: '',
      expanded: false,
      index: 0,
      open: false,
    });
  });
});
