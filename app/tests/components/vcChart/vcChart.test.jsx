import React from 'react';
import { mountWithIntl } from '../../enzyme-test-helpers';
import VcChart from '../../../js/components/vcChart/vcChart';

describe('VcChart', () => {
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcChart />);
    expect(component).toBeDefined();
  });
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcChart minValue={30} maxValue={80} />);
    expect(component).toBeDefined();
  });
});
