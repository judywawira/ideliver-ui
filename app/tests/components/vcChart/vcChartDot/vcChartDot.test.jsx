import React from 'react';
import { mount } from 'enzyme';
import VcChartDot from '../../../../js/components/vcChart/vcChartDot/vcChartDot';

describe('VcChartDot', () => {
  it('renders without value', () => {
    const component = mount(<VcChartDot />);
    expect(component).toBeDefined();
  });
  it('renders with wrong value and console error', () => {
    const component = mount(<VcChartDot value="blah" />);
    expect(component).toBeDefined();
  });
  it('renders regular and with coordinates', () => {
    const component = mount(<VcChartDot value={160} cy={1} cx={2} />);
    expect(component).toBeDefined();
  });
  it('renders irregular', () => {
    const component = mount(<VcChartDot value={160} payload={{ irregular: true }} />);
    expect(component).toBeDefined();
  });
  it('renders alert', () => {
    const component = mount(<VcChartDot value={180} />);
    expect(component).toBeDefined();
  });
  it('renders alert irregular', () => {
    const component = mount(<VcChartDot value={180} payload={{ irregular: true }} />);
    expect(component).toBeDefined();
  });
});
