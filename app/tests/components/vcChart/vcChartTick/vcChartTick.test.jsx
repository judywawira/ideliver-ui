import React from 'react';
import { mountWithIntl } from '../../../enzyme-test-helpers';
import VcChartTick from '../../../../js/components/vcChart/vcChartTick/vcChartTick';

describe('VcChartTick', () => {
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcChartTick payload={{ value: 'test' }} index={0} />);
    expect(component).toBeDefined();
  });
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcChartTick payload={{ value: 'test' }} index={7} visibleTicksCount={9} />);
    expect(component).toBeDefined();
  });
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcChartTick payload={{ value: 'test' }} index={8} visibleTicksCount={9} />);
    expect(component).toBeDefined();
  });
});
