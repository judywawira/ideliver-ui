import React from 'react';
import { mountWithIntl } from '../../enzyme-test-helpers';
import VcClientCard from '../../../js/components/vcClientCard/vcClientCard';

describe('VcClientCard', () => {
  it('renders without crashing', () => {
    const component = mountWithIntl(<VcClientCard
      img="img/demo_photo.png"
      acuity={1}
      givenName="Caroline"
      familyName="Naisaruru"
      age={28}
      companionGivenName="Joel"
      companionFamilyName="Ntimama"
      education="University"
      weeks={39}
      gravidity={1}
      pretermBirths={0}
      termBirths={0}
      abortions={0}
      livingChildren={0}
      riskFactors={['Malaria', 'Allergy']}
      complaints={['Labour signs', 'Headache']}
      task="Give magnesium sulfate"
      patientStatus="admitted"
      nextCheckUp={15}
    />);
    expect(component).toBeDefined();
  });
});
