import React from "react";
import { mountWithIntl, shallowWithIntl } from "../../enzyme-test-helpers";
import VcDialog from "../../../js/components/vcDialog/vcDialog";
import VcDialogContentDiagnosis from "../../../js/components/vcDialogContentDiagnosis/vcDialogContentDiagnosis";
import VcDialogContentSummary from "../../../js/components/vcDialogContentSummary/vcDialogContentSummary";

describe("VcDialog", () => {
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });

  it("renders without crashing", () => {
    const component = shallowWithIntl(
      <VcDialog
        title="Add diagnosis"
        type="Diagnosis"
        onToggle={mockFunc}
        dialogContent={
          <VcDialogContentDiagnosis
            type="Diagnosis"
            filtersSelected={[]}
            filterOptions={["Intact", "Clear", "Meconium", "Bloody"]}
            onSelection={mockFunc}
          />
        }
      />
    );
    expect(component).toBeDefined();
  });

  it("renders without crashing", () => {
    const component = shallowWithIntl(
      <VcDialog
        title="Add summary"
        type="Summary"
        onToggle={mockFunc}
        dialogContent={
          <VcDialogContentSummary
            type="Summary"
            filterComments={["test", "test2"]}
            onComment={mockFunc}
          />
        }
      />
    );
    expect(component).toBeDefined();
  });
});
