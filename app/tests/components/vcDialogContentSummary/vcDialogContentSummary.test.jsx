import React from "react";
import { mountWithIntl, shallowWithIntl } from "../../enzyme-test-helpers";
import VcDialogContentSummary from "../../../js/components/vcDialogContentSummary/vcDialogContentSummary";

describe("VcDialogContentSummary", () => {
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });

  it("renders without crashing", () => {
    const component = shallowWithIntl(
      <VcDialogContentSummary
        type="Summary"
        filterComments={["test", "test2"]}
        onComment={mockFunc}
      />
    );
    expect(component).toBeDefined();
  });

  it("should call the update the state when comment is changed", () => {
    const component = mountWithIntl(
      <VcDialogContentSummary
        type="Summary"
        filterComments={["test", "test2"]}
        onComment={mockFunc}
        comment="Test"
      />
    );

    const button = component.find("VcButton").find("button");

    button.simulate("click");
    expect(component.state()).toEqual({ summary: "Test" });

    const VcTextField = component.find("VcTextField").getElement();

    VcTextField.props.onTextChange("nest");
    expect(component.state()).toEqual({
      summary: "nest"
    });
  });
});
