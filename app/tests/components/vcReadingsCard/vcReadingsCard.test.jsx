import React from 'react';
import { mountWithIntl, shallowWithIntl } from '../../enzyme-test-helpers';
import VcReadingsCard from '../../../js/components/vcReadingsCard/vcReadingsCard';
import VcButton from '../../../js/components/vcButton/vcButton';
import VcChart from '../../../js/components/vcChart/vcChart';

describe('VcReadingsCard', () => {
  it('renders without crashing', () => {
    const component = shallowWithIntl(<VcReadingsCard />);
    expect(component).toBeDefined();
  });

  it('should change state on click on the Avatar', () => {
    const component = mountWithIntl(<VcReadingsCard />);

    const button = component.find('VcButton').at(0);

    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    button.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: true });

    button.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    component.unmount();
  });

  it('should change state on click away', () => {
    const component = mountWithIntl(<VcReadingsCard
      funcComponent={<VcButton value="test" />}
      presentationalComponent={
        <VcChart
          xDataKey="Fetal Heard Rate"
          yDataKey="time"
          topLabelDataKey="liquor"
          bottomLabelDataKey="moulding"
          topLabelColors={{
            I: 'mediumaquamarine',
            C: 'lightsteelblue',
            M: 'burlywood',
            B: 'lightpink',
          }}
          bottomLabelColors={{
            0: 'mediumaquamarine',
            '+': 'lightsteelblue',
            '++': 'burlywood',
            '+++': 'lightpink',
          }}
          value={[
            { time: '9:00' },
            {
              time: '10:00',
              'Fetal Heard Rate': 140,
              liquor: 'C',
              moulding: '+',
            },
            { time: '11:00', 'Fetal Heard Rate': 150 },
            { time: '12:00', 'Fetal Heard Rate': 145, irregular: true },
            {
              time: '13:00',
              'Fetal Heard Rate': 140,
              irregular: true,
              liquor: 'B',
              moulding: '++',
            },
            { time: '14:00', 'Fetal Heard Rate': 160 },
            { time: '15:00', 'Fetal Heard Rate': 180 },
            { time: '16:00', 'Fetal Heard Rate': 180 },
            { time: '17:00' },
          ]}
        />
      }
    />);

    const button = component.find('VcButton').at(0);

    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    button.simulate('click');

    const chart = component.find('VcChart').getElement();

    expect(component.ref('wrappedInstance').state).toEqual({ open: true });

    expect(chart).toBeDefined();

    button.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    // away.props.onClickAway({}, {});
  });
});
