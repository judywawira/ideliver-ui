import React from 'react';
import { mountWithIntl, shallowWithIntl } from '../../enzyme-test-helpers';
import VcSearchButton from '../../../js/components/vcSearchButton/vcSearchButton';

describe('VcSearchButton', () => {
  it('renders without crashing', () => {
    const component = shallowWithIntl(<VcSearchButton />);
    expect(component).toBeDefined();
  });

  it('should change state on click on the Avatar', () => {
    const component = mountWithIntl(<VcSearchButton />);

    const button = component.find('Avatar').at(0);

    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    button.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: true });

    button.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    component.unmount();
  });

  it('should change state on click away', () => {
    const component = mountWithIntl(<VcSearchButton />);

    const avatar = component.find('Avatar').at(0);
    avatar.simulate('click');
    const away = component.find('Clear').getElement();

    expect(component.ref('wrappedInstance').state).toEqual({ open: true });

    away.props.onClick({}, {});

    avatar.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    // away.props.onClickAway({}, {});
  });
});
