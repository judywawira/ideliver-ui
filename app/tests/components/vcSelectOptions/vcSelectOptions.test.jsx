import React from "react";
import { mountWithIntl, shallowWithIntl } from "../../enzyme-test-helpers";
import VcSelectOptions from "../../../js/components/vcSelectOptions/vcSelectOptions";

describe("VcSelectOptions", () => {
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });

  it("renders without crashing", () => {
    const component = shallowWithIntl(
      <VcSelectOptions
        title="Liquor"
        filterOptions={["Intact", "Clear", "Meconium", "Bloody"]}
        selectedOptions={["Intact"]}
        onSelect={mockFunc}
        value={1}
      />
    );
    expect(component).toBeDefined();
  });

  it("should have only one entry in selected Array", () => {
    const component = mountWithIntl(
      <VcSelectOptions
        title="Liquor"
        filterOptions={["Intact", "Clear", "Meconium", "Bloody"]}
        selectedOptions={["Intact"]}
        onSelect={mockFunc}
      />
    );

    const button = component.find("button").at(1);
    expect(component.prop("selectedOptions").length).toBe(1);
    button.simulate("click");
    expect(component.prop("selectedOptions").length).toBe(1);
  });

  it("should have only one entry in selected Array 2", () => {
    const component = mountWithIntl(
      <VcSelectOptions
        title="Liquor"
        filterOptions={["Intact", "Clear", "Meconium", "Bloody"]}
        selectedOptions={["Intact"]}
        onSelect={mockFunc}
      />
    );

    const button = component.find("button").at(0);
    expect(component.prop("selectedOptions").length).toBe(1);
    button.simulate("click");
    expect(component.prop("selectedOptions").length).toBe(0);
  });
});
