import React from 'react';
import { mount } from 'enzyme';
import VcSlider from '../../../js/components/vcSlider/vcSlider';

describe('VcSlider', () => {
  it('renders without crashing', () => {
    const component = mount(<VcSlider value={99} />);
    expect(component).toBeDefined();
  });
});
