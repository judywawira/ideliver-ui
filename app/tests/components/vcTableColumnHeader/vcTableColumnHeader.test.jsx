import React from 'react';
import VcTableColumnHeader from '../../../js/components/vcTableColumnHeader/vcTableColumnHeader';
import { mountWithIntl, shallowWithIntl } from '../../enzyme-test-helpers';

describe('VcTableColumnHeader', () => {
  let mockFunc,
    mockFunc2;
  beforeEach(() => {
    mockFunc = jest.fn();
    mockFunc2 = jest.fn();
  });

  it('renders without crashing', () => {
    const component = shallowWithIntl(<VcTableColumnHeader
      value="filterAndSort"
      sortable
      filterOptions={['test0', 'test1', 'test2', 'test3', 'test4', 'test5']}
      filtersSelected={[0, 4, 5]}
    >
        test
    </VcTableColumnHeader>);
    expect(component).toBeDefined();
  });

  it('should change state on clicking button', () => {
    const component = mountWithIntl(<VcTableColumnHeader
      value="filterAndSort"
      sortable
      filterOptions={['test0', 'test1', 'test2', 'test3', 'test4', 'test5']}
      filtersSelected={[0, 4, 5]}
    >
        test
    </VcTableColumnHeader>);
    const button = component.find('Button').at(0);

    expect(component.ref('wrappedInstance').state).toEqual({ open: false });
    button.simulate('click');

    expect(component.ref('wrappedInstance').state).toEqual({ open: true });
  });

  it('should change state on click away', () => {
    const component = mountWithIntl(<VcTableColumnHeader
      onSort={mockFunc2}
      onFilter={mockFunc}
      value="filterAndSort"
      sortable
      filterOptions={['test0', 'test1', 'test2', 'test3', 'test4', 'test5']}
      filtersSelected={[0, 4, 5]}
    >
        test
    </VcTableColumnHeader>);

    const openButton = component.find('Button').at(0);

    openButton.simulate('click');

    const away = component.find('ClickAwayListener').getElement();

    expect(component.ref('wrappedInstance').state).toEqual({ open: true });

    away.props.onClickAway({}, {});

    openButton.simulate('click');
    expect(component.ref('wrappedInstance').state).toEqual({ open: false });

    away.props.onClickAway({}, {});
  });

  it('should fire sort action', () => {
    const component = mountWithIntl(<VcTableColumnHeader onSort={mockFunc} value="Sort" sortable>
        test
    </VcTableColumnHeader>);

    const sortButton = component.find('Button').at(1);
    sortButton.simulate('click');

    expect(mockFunc.mock.calls.length).toBe(1);
  });
});
