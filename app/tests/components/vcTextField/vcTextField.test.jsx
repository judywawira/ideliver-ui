import React from "react";
import { mountWithIntl, shallowWithIntl } from "../../enzyme-test-helpers";
import VcTextField from "../../../js/components/vcTextField/vcTextField";

describe("vcTextField", () => {
  let mockFunc;
  beforeEach(() => {
    mockFunc = jest.fn();
  });

  it("renders without crashing", () => {
    const component = shallowWithIntl(
      <VcTextField value="Test" label="Add summary" onTextChange={mockFunc} />
    );
    expect(component).toBeDefined();
  });

  it("should call the update the state when comment is changed", () => {
    const component = mountWithIntl(
      <VcTextField value="Test" label="Add summary" onTextChange={mockFunc} />
    );

    expect(component.state()).toEqual({ comment: "Test" });

    const TextField = component
      .find("VcTextField")
      .find("TextField")
      .getElement();
    TextField.props.onChange({ target: { value: "comment" } });
    expect(component.state()).toEqual({ comment: "comment" });
  });
});
