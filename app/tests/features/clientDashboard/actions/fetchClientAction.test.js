import fetchClientAction, { FETCH_CLIENT_ACTION } from '../../../../js/features/clientDashboard/actions/fetchClientAction';

describe('fetchClientAction', () => {
  it('should return the correct action', () => {
    const result = fetchClientAction('test', 2);
    expect(result).toEqual({
      type: FETCH_CLIENT_ACTION,
      url: 'test',
      visitId: 2,
    });
  });
});
