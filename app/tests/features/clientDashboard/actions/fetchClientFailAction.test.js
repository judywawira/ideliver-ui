import fetchClientFailAction, { FETCH_CLIENT_FAILED } from '../../../../js/features/clientDashboard/actions/fetchClientFailAction';

describe('fetchClientFailAction', () => {
  it('should return the correct action', () => {
    const result = fetchClientFailAction('test');
    expect(result).toEqual({
      type: FETCH_CLIENT_FAILED,
      payload: 'test',
    });
  });
});
