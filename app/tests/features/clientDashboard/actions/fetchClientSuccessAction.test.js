import fetchClientSuccessAction, { FETCH_CLIENT_SUCCEEDED } from '../../../../js/features/clientDashboard/actions/fetchClientSuccessAction';

describe('fetchClientSuccessAction', () => {
  it('should return the correct action', () => {
    const result = fetchClientSuccessAction('test');
    expect(result).toEqual({
      type: FETCH_CLIENT_SUCCEEDED,
      payload: 'test',
    });
  });
});
