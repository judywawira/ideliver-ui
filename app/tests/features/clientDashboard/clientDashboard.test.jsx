import React from 'react';
import ClientDashboard from '../../../js/features/clientDashboard/clientDashboard';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';

describe('ClientDashboard', () => {
  let mockFunc1;
  beforeEach(() => {
    mockFunc1 = jest.fn();
  });

  it('should render', () => {
    const result = [];
    const component = mountWithRouterAndIntl(<ClientDashboard dispatchFetchClientAction={mockFunc1} location={{ pathname: 'test' }} />);

    expect(component).toBeDefined();
  });
});
