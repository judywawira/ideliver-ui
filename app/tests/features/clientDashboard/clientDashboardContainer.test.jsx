import React from 'react';
import { fromJS } from 'immutable';
import { shallowWithStore } from '../../enzyme-test-helpers';
import { createMockStore } from 'redux-test-utils';

import ClientDashboardContainer from '../../../js/features/clientDashboard/clientDashboardContainer';

describe('ClientDashboardContainer', () => {
  let store,
    component,
    testState;

  beforeEach(() => {
    testState = {
      Visits: { details: fromJS({ 1: { uuid: 1, name: 'test' } }) },
    };
    store = createMockStore(testState);
    component = shallowWithStore(
      <ClientDashboardContainer location={{ pathname: '0123456789' }} />,
      store,
    );
  });

  it('should render without throwing an error', () => {
    expect(component).toBeDefined();
  });

  it('demo action should get dispatched', () => {
    component.props().dispatchFetchClientAction();

    const actions = store.getActions();
    expect(actions).toEqual([
      { url: '/openmrs/ws/rest/v1/', visitId: '89', type: 'FETCH_CLIENT_ACTION' },
    ]);
  });
});
