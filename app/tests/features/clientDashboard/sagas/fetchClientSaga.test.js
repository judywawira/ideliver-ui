import React from 'react';
import { FormattedDate } from 'react-intl';
import { put, call } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';
import fetchClientSaga from '../../../../js/features/clientDashboard/sagas/fetchClientSaga';
import fetchClientSuccessAction from '../../../../js/features/clientDashboard/actions/fetchClientSuccessAction';
import fetchClientFailAction from '../../../../js/features/clientDashboard/actions/fetchClientFailAction';
import fetchClientAction from '../../../../js/features/clientDashboard/actions/fetchClientAction';
import { fetchUrl } from '../../../../js/api';

const action = fetchClientAction('test', 1);

describe('Fetch Client flow', () => {
  const generator = cloneableGenerator(fetchClientSaga)(action);
  expect(generator.next().value).toEqual(call(fetchUrl, `${action.url}visit/${action.visitId}?v=custom:(uuid,patient:(person))`));

  test('fetch success with 3 names', () => {
    const clone = generator.clone();
    expect(clone.next({
      uuid: '30a3e000-473b-4274-bdac-c9df86648cca',
      patient: {
        person: {},
      },
    }).value).toEqual(put(fetchClientSuccessAction({
      '30a3e000-473b-4274-bdac-c9df86648cca': {
        uuid: '30a3e000-473b-4274-bdac-c9df86648cca',
        patient: {
          person: {},
        },
      },
    })));
    expect(clone.next().done).toEqual(true);
  });

  test('fetch error', () => {
    const clone = generator.clone();
    expect(clone.throw({ message: 'error' }).value).toEqual(put(fetchClientFailAction('error')));
    expect(clone.next().done).toEqual(true);
  });
});
