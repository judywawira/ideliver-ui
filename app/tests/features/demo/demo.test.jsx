import React from 'react';
import Demo from '../../../js/features/demo/demo';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';

describe('Demo', () => {
  let mockFunc1,
    mockFunc2;
  beforeEach(() => {
    mockFunc1 = jest.fn();
    mockFunc2 = jest.fn();
  });

  it('should run the dispatchDemoAction function', () => {
    const component = mountWithRouterAndIntl(<Demo
      location={{ pathname: '/tasks' }}
      name="test"
      dispatchDemoAction={mockFunc1}
      dispatchListChangeAction={mockFunc2}
    />);

    const firstButton = component.find('button').at(0);

    firstButton.simulate('click');
    expect(mockFunc1.mock.calls.length).toBe(1);
    expect(mockFunc2.mock.calls.length).toBe(0);
  });

  it('should run the dispatchListChangeAction function', () => {
    const component = mountWithRouterAndIntl(<Demo
      location={{ pathname: '/tasks' }}
      name="test"
      dispatchDemoAction={mockFunc1}
      dispatchListChangeAction={mockFunc2}
    />);

    const secondButton = component.find('button').at(1);

    secondButton.simulate('click');
    expect(mockFunc2.mock.calls.length).toBe(1);
    expect(mockFunc1.mock.calls.length).toBe(0);
  });

  it('should trigger onChange of the input and change the state', () => {
    const component = mountWithRouterAndIntl(<Demo
      location={{ pathname: '/tasks' }}
      name="test"
      dispatchDemoAction={mockFunc1}
      dispatchListChangeAction={mockFunc2}
    />);

    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });
    const input = component.find('input').at(0);
    input.simulate('change', { target: { value: 'asdas' } });
    expect(component.state()).toEqual({
      textField: 'asdas',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });

    const toggle = component
      .find('VcToggle')
      .find('input')
      .at(0);
    toggle.simulate('change', true);
    expect(component.state()).toEqual({
      textField: 'asdas',
      toggle: true,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });
    component.unmount();
  });

  it('should trigger onChange of the VcDropDown input and change the state', () => {
    const component = mountWithRouterAndIntl(<Demo
      location={{ pathname: '/tasks' }}
      name="test"
      dispatchDemoAction={mockFunc1}
      dispatchListChangeAction={mockFunc2}
    />);
    const dropDown = component
      .find('VcNumericDropDown')
      .find('VcButton')
      .at(0);

    dropDown.simulate('click');
    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 100,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });

    const dropDownSub = component
      .find('VcNumericDropDown')
      .find('VcButton')
      .at(1);

    dropDownSub.simulate('click');
    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });

    const dropDownInput = component
      .find('VcNumericDropDown')
      .find('input')
      .at(0);

    dropDownInput.simulate('change');
    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });
  });

  it('should trigger onChange of the VcSelectOptions select multiple input and change the state', () => {
    const component = mountWithRouterAndIntl(<Demo
      location={{ pathname: '/tasks' }}
      name="test"
      dispatchDemoAction={mockFunc1}
      dispatchListChangeAction={mockFunc2}
    />);
    const button = component
      .find('VcSelectOptions')
      .at(0)
      .find('VcButton')
      .at(0);

    button.simulate('click');
    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: ['Intact'],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });

    button.simulate('click');
    expect(component.state()).toEqual({
      textField: 'test',
      toggle: false,
      value: 80,
      selectedOptions: [],
      filtersSelected: [],
      filterOptions: [
        'Pre-eclampsia',
        'Infection',
        'Anemia',
        'Erythrocytosis',
        'Shock',
        'Test1',
        'Test2',
        'Test3',
        'Test4',
        'Test5',
        'Shock2',
        'Test12',
        'Test22',
        'Test32',
        'Test42',
        'Test52',
      ],
      filterComments: [],
    });
  });
});
