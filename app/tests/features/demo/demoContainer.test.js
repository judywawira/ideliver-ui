import React from 'react';
import { shallowWithStore } from '../../enzyme-test-helpers';
import { createMockStore } from 'redux-test-utils';

import DemoContainer from '../../../js/features/demo/demoContainer';

describe('DemoContainer', () => {
  let store,
    component,
    testState;

  beforeEach(() => {
    testState = {
      Demo: { name: 'test' },
    };
    store = createMockStore(testState);
    component = shallowWithStore(<DemoContainer />, store);
  });

  it('should render without throwing an error', () => {
    expect(component).toBeDefined();
  });

  it('demo action should get dispatched', () => {
    component.props().dispatchDemoAction('test');

    const actions = store.getActions();
    expect(actions).toEqual([{ payload: 'test', type: 'DEMO_ACTION' }]);
  });

  it('listChanged action should get dispatched', () => {
    component.props().dispatchListChangeAction();

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        payload: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
        type: 'LIST_CHANGE_ACTION',
      },
    ]);
  });
});
