import { fromJS } from 'immutable';
import DemoReducer from '../../../js/features/demo/demoReducer';
import DemoAction from '../../../js/features/demo/actions/demoAction';
import ListChangeAction from '../../../js/features/demo/actions/listChangeAction';
import initialState from '../../../js/initialState';

describe('DemoReducer', () => {
  it('should return the initial state', () => {
    expect(DemoReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle DEMO_ACTION', () => {
    const resultState = {
      name: 'Test Payload',
    };
    expect(DemoReducer({}, DemoAction('Test Payload'))).toEqual(resultState);
  });

  it('should handle LIST_CHANGE_ACTION', () => {
    const resultState = fromJS(['a', 'b', 'c', 'd']);
    expect(DemoReducer({}, ListChangeAction(['a', 'b', 'c', 'd']))).toEqual({
      listOfSomething: resultState,
    });
  });
});
