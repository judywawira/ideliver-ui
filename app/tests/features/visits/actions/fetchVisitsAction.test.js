import fetchVisitsAction, { FETCH_VISITS_ACTION } from '../../../../js/features/visits/actions/fetchVisitsAction';

describe('fetchVisitsAction', () => {
  it('should return the correct action', () => {
    const result = fetchVisitsAction('test', 2, 3);
    expect(result).toEqual({
      type: FETCH_VISITS_ACTION,
      url: 'test',
      pageSize: 2,
      pageIndex: 3,
    });
  });
});
