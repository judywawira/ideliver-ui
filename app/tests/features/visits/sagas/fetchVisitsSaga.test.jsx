import React from 'react';
import { FormattedDate } from 'react-intl';
import { put, call } from 'redux-saga/effects';
import { cloneableGenerator } from 'redux-saga/utils';
import fetchVisitsSaga from '../../../../js/features/visits/sagas/fetchVisitsSaga';
import fetchVisitsSuccessAction from '../../../../js/features/visits/actions/fetchVisitsSuccessAction';
import fetchVisitsFailAction from '../../../../js/features/visits/actions/fetchVisitsFailAction';
import fetchVisitsAction from '../../../../js/features/visits/actions/fetchVisitsAction';
import { fetchUrl } from '../../../../js/api';

const action = fetchVisitsAction('test', 1, 0);

describe('Fetch Visits flow', () => {
  const generator = cloneableGenerator(fetchVisitsSaga)(action);
  expect(generator.next().value).toEqual(call(fetchUrl, `${action.url}visit/?v=custom:(uuid,startDatetime,patient:(uuid,display))`));

  test('fetch success with 3 names', () => {
    const clone = generator.clone();
    expect(clone.next({
      results: [
        {
          uuid: '30a3e000-473b-4274-bdac-c9df86648cca',
          startDatetime: '2018-03-29T11:11:55.000-0400',
          patient: {
            uuid: '01ec3837-3c19-4a5c-aa6b-cca2157e9083',
            display: '10006H - firstName 9h9 lastName',
          },
        },
      ],
    }).value).toEqual(put(fetchVisitsSuccessAction([
      {
        id: '30a3e000-473b-4274-bdac-c9df86648cca',
        mrn: '10006H',
        givenName: 'firstName',
        familyName: 'lastName',
        admissionTime: (
          <FormattedDate
            value="2018-03-29T11:11:55.000-0400"
            month="long"
            day="2-digit"
            hour="2-digit"
            minute="2-digit"
            second="2-digit"
          />
        ),
      },
    ])));
    expect(clone.next().done).toEqual(true);
  });

  test('fetch success with 2 names', () => {
    const clone = generator.clone();
    expect(clone.next({
      results: [
        {
          uuid: '30a3e000-473b-4274-bdac-c9df86648cca',
          startDatetime: '2018-03-29T11:11:55.000-0400',
          patient: {
            uuid: '01ec3837-3c19-4a5c-aa6b-cca2157e9083',
            display: '10006H - firstName lastName',
          },
        },
      ],
    }).value).toEqual(put(fetchVisitsSuccessAction([
      {
        id: '30a3e000-473b-4274-bdac-c9df86648cca',
        mrn: '10006H',
        givenName: 'firstName',
        familyName: 'lastName',
        admissionTime: (
          <FormattedDate
            value="2018-03-29T11:11:55.000-0400"
            month="long"
            day="2-digit"
            hour="2-digit"
            minute="2-digit"
            second="2-digit"
          />
        ),
      },
    ])));
    expect(clone.next().done).toEqual(true);
  });

  test('fetch success with less records than page size', () => {
    const action2 = fetchVisitsAction('test', 2, 0);
    const generator2 = cloneableGenerator(fetchVisitsSaga)(action2);
    expect(generator2.next().value).toEqual(call(fetchUrl, `${action2.url}visit/?v=custom:(uuid,startDatetime,patient:(uuid,display))`));

    expect(generator2.next({
      results: [
        {
          uuid: '30a3e000-473b-4274-bdac-c9df86648cca',
          startDatetime: '2018-03-29T11:11:55.000-0400',
          patient: {
            uuid: '01ec3837-3c19-4a5c-aa6b-cca2157e9083',
            display: '10006H - firstName 9h9 lastName',
          },
        },
      ],
    }).value).toEqual(put(fetchVisitsSuccessAction([
      {
        id: '30a3e000-473b-4274-bdac-c9df86648cca',
        mrn: '10006H',
        givenName: 'firstName',
        familyName: 'lastName',
        admissionTime: (
          <FormattedDate
            value="2018-03-29T11:11:55.000-0400"
            month="long"
            day="2-digit"
            hour="2-digit"
            minute="2-digit"
            second="2-digit"
          />
        ),
      },
    ])));
    expect(generator2.next().done).toEqual(true);
  });

  test('fetch error', () => {
    const clone = generator.clone();
    expect(clone.throw({ message: 'error' }).value).toEqual(put(fetchVisitsFailAction('error')));
    expect(clone.next().done).toEqual(true);
  });
});
