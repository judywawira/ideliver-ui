import React from 'react';
import Visits from '../../../js/features/visits/visits';
import { mountWithRouterAndIntl } from '../../enzyme-test-helpers';

describe('Visits', () => {
  let mockFunc1,
    mockFunc2;
  beforeEach(() => {
    mockFunc1 = jest.fn();
    mockFunc2 = jest.fn();
  });

  it('should render', () => {
    const result = [];
    const component = mountWithRouterAndIntl(<Visits
      dispatchFetchVisitsAction={mockFunc1}
      history={result}
      value={[
        {
          id: '010',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '010',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '001',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '001',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '002',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '002',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '003',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '003',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '004',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '004',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '005',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '005',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '006',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '006',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '007',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '007',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '008',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '008',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '009',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '009',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
        {
          id: '011',
          givenName: 'Martin',
          familyName: 'Zakov',
          mrn: '011',
          status: 'needs attention',
          acuity: 1,
          admissionTime: '03/13/2018',
          diagnosis: 'Trololo',
          alerts: null,
        },
      ]}
    />);

    expect(component).toBeDefined();

    const table = component.find('VcTable').getElement();
    expect(mockFunc1.mock.calls.length).toBe(1);

    table.props.onChangePage({}, 1);
    expect(mockFunc1.mock.calls.length).toBe(2);

    table.props.onChangePage({}, 0);
    expect(mockFunc1.mock.calls.length).toBe(3);

    table.props.onChangeRowsPerPage({ target: { value: 20 } }, {});
    expect(mockFunc1.mock.calls.length).toBe(4);

    table.props.onRowClick({}, 1);
  });
});
