import { fromJS, List, Map } from 'immutable';
import VisitsReducer from '../../../js/features/visits/visitsReducer';
import fetchVisitsSuccessAction from '../../../js/features/visits/actions/fetchVisitsSuccessAction';
import fetchVisitsFailAction from '../../../js/features/visits/actions/fetchVisitsFailAction';

describe('VisitsReducer', () => {
  it('should return the initial state', () => {
    expect(VisitsReducer(undefined, {})).toEqual({
      list: new List(),
      validation: new List(),
      details: new Map(),
    });
  });

  it('should handle fetchVisitsSuccessAction', () => {
    expect(VisitsReducer({}, fetchVisitsSuccessAction([1, 2, 3]))).toEqual({
      list: fromJS([1, 2, 3]),
    });
  });

  it('should handle fetchVisitsFailAction', () => {
    expect(VisitsReducer({}, fetchVisitsFailAction(['error']))).toEqual({
      validation: fromJS(['error']),
    });
  });
});
