import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Main from '../js/main';
import { MemoryRouter } from 'react-router-dom';

describe('Main', () => {
  it('renders without crashing', () => {
    const component = shallow(<MemoryRouter>
      <Main />
    </MemoryRouter>);
    expect(component).toBeDefined();
  });
});
