import { createStore } from 'redux';
import RootReducer from '../js/rootReducer';
import initialState from '../js/initialState';

describe('RootReducer', () => {
  it('should return the initial state', () => {
    const store = createStore(RootReducer);
    expect(store.getState().Demo).toEqual(initialState);
  });
});
