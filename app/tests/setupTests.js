import Enzyme from 'enzyme';
// import Adapter from 'enzyme-adapter-react-16';
// Temporary solution while new ver of Enzyme comes out that support react 16.3.x
import Adapter from './reactSixteenAdapter';

Enzyme.configure({ adapter: new Adapter() });
