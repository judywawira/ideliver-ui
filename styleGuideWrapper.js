import React from 'react';
import { IntlProvider } from 'react-intl';

export default class Wrapper extends React.Component {
  render() {
    return <IntlProvider locale="en">{this.props.children}</IntlProvider>;
  }
}
